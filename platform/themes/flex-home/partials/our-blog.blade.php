@php

      use Carbon\Carbon;

      if(app()->getLocale()=="ar"){
           $blog=\Botble\Blog\Models\Post::where("is_featured",1)->where('default_ar',1)->orderBy("id","Desc")->limit(5)->get();
      } else {
           $blog=\Botble\Blog\Models\Post::where("is_featured",1)->where('default_ar',0)->orderBy("id","Desc")->limit(5)->get();
      }

    $folderName="storage";
    $i=0;

function getImageSrc($folderName,$img){

    $result=null;
    $j=0;
    $size=['big','medium','small','thumb'];

    while ($j<count($size)){

       $result=RvMedia::getImageUrl($img, $size[$j], false, RvMedia::getDefaultImage());
       $imageName=basename($result);

       if(file_exists($folderName."/". $imageName)) return $result;
        $j++;
    }

    return $result;
}

@endphp

@if(isset($blog) && !empty($blog))
    <div class="container-fluid mt-5">
        <div>
            <p class="blog-title">{{__("Our Blog")}}</p>
        </div>
        <div id="gallery">
            @foreach($blog as $b)

                @php
                    $dayNumber = date('d', strtotime($b->created_at));
                    $month = date('M', strtotime($b->created_at));
                    $slug=\Botble\Slug\Models\Slug::where("reference_id",$b->id)->first();

                  if(isset($b->image) && !empty($b->image)){
                        $extension_pos = strrpos($b->image, '.');
                        $img = substr($b->image, 0, $extension_pos) ."-150x150" . substr($b->image, $extension_pos);
                        }
                @endphp
                @if($i==0)
                    <div class="pics animation all 1 custom-col-6">
                        <div class="circle-date color_black">
                            <label>
                                <span class="font-size-16"> {{$dayNumber}}</span><br>{{$month}}
                            </label>
                        </div>
                        @if(isset($b->image) && !empty($b->image))
                            <img class="img-fluid img-fluid-big" src="{{ getImageSrc($folderName,$b->image)}}" alt="Card image cap">
                        @else
                            <img class="img-fluid img-fluid-big" src="{{url('storage/defult.png')}}" alt="Card image cap">
                        @endif
                        <div class="centered centered-blog">
                            {{$b->name}}
                            <br>
                            <a href="{{ $b->url }}" type="button"
                               class="btn btn-read-more mt-2 read-more-big-project">   {{__("Read More")}}
                            </a>
                        </div>
                    </div>
                @else
                <!-- Grid column -->
                    @if($i==1)
                        <div class="custom-col-6">
                            <div>
                                @endif
                                <div class="pics animation all 2 custom-col-6">
                                    <div class="circle-date-small color_black">
                                        <label><span class="font-size-16">{{$dayNumber}} </span><br>{{$month}}</label>
                                    </div>

                                    @if(isset($b->image) && !empty($b->image))
                                      <img class="img-fluid img-fluid-small" src="{{ getImageSrc($folderName,$b->image)}}" alt="Card image cap">
                                    @else
                                        <img class="img-fluid img-fluid-small" src="{{url('storage/defult.png')}}" alt="Card image cap">
                                    @endif
                                    <div class="centered centered-small  centered-blog-small">{{$b->name}}
                                        <br>
                                        <a href="{{ $b->url }}" type="button"
                                           class="btn btn-read-more mt-2 read-more-small-project color_black">
                                            {{__("Read More")}}
                                        </a>
                                    </div>
                                </div>
                                @endif
                                @php $i++ @endphp
                                @endforeach
                                @if($i>=1)
                            </div>
                        </div>
                    @endif
        </div>
    </div>
@endif
