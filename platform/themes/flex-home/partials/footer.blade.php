<!--FOOTER-->
<footer>
    <br>
    <div class="container-fluid w90">
        <div class="row">
            <div class="col-sm-3" style="text-align: center;">
                @if (theme_option('logo'))
                    <p>
                        <a href="{{ route('public.index') }}">
                            <img src="{{ RvMedia::getImageUrl(theme_option('logo'))  }}" style="max-height: 71px" alt="{{ theme_option('site_name') }}">
                        </a>
                    </p>
                    <div class="social-icons">
                        <a href="https://www.instagram.com/sama_properties/"><i class="fab fa-instagram"></i></a>
                        <a href="https://www.facebook.com/profile.php?id=100084601886019"><i
                                class="fab fa-facebook-f"></i></a>
                        <a href="https://www.tiktok.com/@sama_emirates?_t=8WH7TVgol9d&_r=1">
                            <svg class='fontawesomesvg' xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
                                <!--! Font Awesome Free 6.2.0 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license/free (Icons: CC BY 4.0, Fonts: SIL OFL 1.1, Code: MIT License) Copyright 2022 Fonticons, Inc. -->
                                <path
                                    d="M448,209.91a210.06,210.06,0,0,1-122.77-39.25V349.38A162.55,162.55,0,1,1,185,188.31V278.2a74.62,74.62,0,1,0,52.23,71.18V0l88,0a121.18,121.18,0,0,0,1.86,22.17h0A122.18,122.18,0,0,0,381,102.39a121.43,121.43,0,0,0,67,20.14Z"/>
                            </svg>
                        </a>
                    </div>
                @endif

            </div>
            <div class="col-sm-6 col-lg-7 padtop10">
                <div class="row">
                    @if(app()->getLocale()=="en")
                        {!! dynamic_sidebar('footer_sidebar') !!}
                    @else
                        @php
                            $footerValue=array(
                              "الصفحة الرئيسية"=>url("/ar"),
                              "للبيع"=>url("/ar/buy-properties"),
                              "للإيجار"=>url("/ar/rent-properties"),
                              "المشاريع"=>url("/ar/projects"),
                              "المناطق"=>url("/ar/areas"),
                              "المدونة"=>url("/ar/blog"),
                              "من نحن"=>url("/ar/about-us"),
                              "اتصل بنا"=>url("/ar/contact"),
                              "الأسئلة الأكثر تكراراً"=>url("/ar/faq"),
                              "سياسة الخصوصية"=>url("/ar/privacypolicy")
                              )
                        @endphp
                        <div class="col-sm-4">
                            <div class="menufooter">
                                <h4>Custom Menu</h4>
                                <ul>
                                    @foreach($footerValue as $key=>$value)
                                        <li>
                                            <a href="{{$value}}">
                                                <span>{{$key}}</span>
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>

                            </div>
                        </div>
                    @endif
                </div>
            </div>
            @php
                if(app()->getLocale() =="ar")
                     $url="/ar/login";
                        else
                     $url="/login";
            @endphp
            <div class="col-sm-3 col-md-auto col-lg-2">
                <div class="row">
                    <div class="col" style="text-align: center;"><a href="{{url($url)}}" class="btn btn-primary"
                                                                    style="background: rgb(4,78,138);width: 119px;padding: 12px;border-radius: 0px;font-size: 14px;font-weight: 500;">{{__('Login')}} </a>
                    </div>
                </div>
                {{--                <div class="row">--}}
                {{--                    <div class="col" style="text-align: center;"><a href="{{url('/sell-with-us')}}" class="btn btn-primary" type="button" style="background: rgba(4,78,138,0);width: 119px;padding: 12px;border-radius: 0px;font-weight: 500;font-size: 14px;margin-top: 20px;color: rgb(4,78,138);border-width: 3px;border-color: rgb(4,78,138);">{{__('Sell with us')}} </a></div>--}}
                {{--                </div>--}}
            </div>
        </div>
        <div class="row" style="display: none">
            <div class="col-12">
                {!! Theme::partial('language-switcher') !!}
            </div>
        </div>
        <div class="copyright">
            <div class="col-sm-12">
                <p class="text-center">
                    {!! BaseHelper::clean(theme_option('copyright')) !!}
                </p>
            </div>
        </div>
    </div>
</footer>
<!--FOOTER-->

<script>
    window.trans = {
        "Price": "{{ __('Price') }}",
        "Number of rooms": "{{ __('Number of rooms') }}",
        "Number of rest rooms": "{{ __('Number of rest rooms') }}",
        "Square": "{{ __('Square') }}",
        "No property found": "{{ __('No property found') }}",
        "million": "{{ __('million') }}",
        "billion": "{{ __('billion') }}",
        "in": "{{ __('in') }}",
        "Added to wishlist successfully!": "{{ __('Added to wishlist successfully!') }}",
        "Removed from wishlist successfully!": "{{ __('Removed from wishlist successfully!') }}",
        "I care about this property!!!": "{{ __('I care about this property!!!') }}",
    }
    window.themeUrl = '{{ Theme::asset()->url('') }}';
    window.siteUrl = '{{ url('') }}';
    window.currentLanguage = '{{ App::getLocale() }}';
</script>

<!--END FOOTER-->

<div class="action_footer">
    <a href="#" class="cd-top"
       @if (!Theme::get('hotlineNumber') && !theme_option('hotline')) style="top: -40px;" @endif><i
            class="fas fa-arrow-up"></i></a>
    @if (Theme::get('hotlineNumber') || theme_option('hotline'))
        <a href="tel:{{ Theme::get('hotlineNumber') ?: theme_option('hotline') }}"
           style="color: white;font-size: 17px;"><i class="fas fa-phone"></i>
            <span class="mobile-all">  &nbsp;{{ Theme::get('hotlineNumber') ?: theme_option('hotline') }}</span></a>
    @endif
</div>


<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery.jssocials/1.4.0/jssocials.min.js"></script>

<link type="text/css" rel="stylesheet" href="https://cdn.jsdelivr.net/jquery.jssocials/1.4.0/jssocials.css"/>

<link type="text/css" rel="stylesheet" href="https://cdn.jsdelivr.net/jquery.jssocials/1.4.0/jssocials-theme-flat.css"/>
<link type="text/css" rel="stylesheet"
      href="https://cdn.jsdelivr.net/jquery.jssocials/1.4.0/jssocials-theme-classic.css"/>
<link type="text/css" rel="stylesheet"
      href="https://cdn.jsdelivr.net/jquery.jssocials/1.4.0/jssocials-theme-minima.css"/>
<link type="text/css" rel="stylesheet"
      href="https://cdn.jsdelivr.net/jquery.jssocials/1.4.0/jssocials-theme-plain.css"/>
<script src="https://cdn.jsdelivr.net/picturefill/2.3.1/picturefill.min.js"></script>

{!! Theme::footer() !!}
</body>
</html>
