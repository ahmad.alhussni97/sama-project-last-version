@php

if(app()->getLocale() =="ar"){
    $urlBuy=Request::root().'/ar/buy-properties';
    $urlRent=Request::root().'/ar/rent-properties';
} else{
    $urlBuy=Request::root().'/buy-properties';
    $urlRent=Request::root().'/rent-properties';
}


@endphp
<div class="container frequently-searched">
    <div class="row">
        <div class="col-sm-12 col-lg-6">
            <h2 style="font-weight:700">{{__('Frequently Searched')}} </h2>
            <div class="row">
                <div class="col-sm-6 col-lg-6">
                    <ul class="list-freq-search">

                        <li class="li-feq-list">
                            <a href="{{$urlRent.'?location=Abu Dhabi&category_id=1'}}">{{__('Apartment for rent in Abu Dhabi')}}</a>
                        </li>

                        <li class="li-feq-list">
                            <a href="{{$urlBuy.'?location=Abu Dhabi&category_id=1'}}">{{__('Apartment for sale in Abu Dhabi')}}</a>
                        </li>

                        <li class="li-feq-list">
                            <a href="{{$urlBuy.'?location=Abu Dhabi&category_id=17'}}">{{__('Shops for sale in Abu Dhabi')}}</a>
                        </li>

                        <li class="li-feq-list">
                            <a href="{{$urlRent.'?location=Abu Dhabi&category_id=2'}}">{{__('Villas for rent in Abu Dhabi')}}</a>
                        </li>

                        <li class="li-feq-list">
                            <a href="{{$urlBuy.'?location=Abu Dhabi&category_id=2'}}">{{__('Villas for sale in Abu Dhabi')}}</a>
                        </li>
                    </ul>
                </div>
                <div class="col-sm-6 col-lg-6">
                    <ul class="list-freq-search">

                        <li class="li-feq-list">
                            <a href="{{$urlBuy.'?location=Abu Dhabi&category_id=12'}}">{{__('Penthouse for sale in Abu Dhabi')}}</a>
                        </li>

                        <li class="li-feq-list">
                            <a href="{{$urlBuy.'?location=Dubai&category_id=1'}}">{{__('Apartment for sale in Dubai')}}</a>
                        </li>

                        <li class="li-feq-list">
                            <a href="{{$urlRent.'?location=Abu Dhabi&category_id=8'}}">{{__('Studio for rent in Abu Dhabi')}}</a>
                        </li>

                        <li class="li-feq-list">
                            <a href="{{$urlBuy.'?location=Abu Dhabi&category_id=10'}}">{{__('Townhouse for sale in Abu Dhabi')}}</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col positions-faq" >

            <img src="{{asset("storage/footer-img.jpeg")}}"
                 style="border-radius: 155px;width: 100%;height: 324px;border: 20px solid white;box-shadow: 0px 0px 5px rgba(33,37,41,0.54);">
        </div>
    </div>
</div>
