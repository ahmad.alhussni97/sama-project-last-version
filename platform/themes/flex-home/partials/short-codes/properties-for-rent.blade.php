<div class="container-fluid w90">
    <div class="homehouse padtop30 projecthome">
        <div class="row">
            <div class="col-12 rent-hover rent-hover-custom">
                <a href="{{url()->current().'/rent-properties'}}">
                    <h2 class="color_black">{!! __(BaseHelper::clean($title)) !!}</h2>
                </a>
                @if ($subtitle)
                    <p>{!! __(BaseHelper::clean($subtitle)) !!}</p>
                @endif
            </div>
        </div>
        <property-component type="rent" url="{{ route('public.ajax.properties') }}"></property-component>
    </div>
</div>
