<div class="padtop50">
    <div class="box_shadow custom-for-sale">
        <div class="container-fluid w90">
            <div class="homehouse projecthome">
                <div class="row">
                    <div class="col-12 sale-hover">
                        @if(BaseHelper::clean($title) == "Our Featured Properties")
                        <h2 style="text-align: center;" class="color_black">{!! __(BaseHelper::clean($title)) !!}</h2>
                        @else
                        <a href="{{url()->current().'/buy-properties'}}"><h2  class="color_black">{!! __(BaseHelper::clean($title)) !!}</h2></a>
                        @endif
                        @if ($subtitle)
                            <p>{!! __(BaseHelper::clean($subtitle)) !!}</p>
                        @endif

                    </div>
                </div>
                <property-component type="sale" url="{{ route('public.ajax.properties') }}"></property-component>
            </div>
        </div>
    </div>
</div>
