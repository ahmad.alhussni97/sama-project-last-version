<div id="contact">
    <section>
        <div class="container">
            <div class="row">
                <div class="desc-contact-us text-left">
                    @if(app()->getLocale()=="en")
                        The ball is in our court, which means that it is our responsibility to make you feel
                        comfortable, be
                        responsive, and offer you the best real estate services. Consequently, we have extensively
                        developed
                        ourselves and our services, notably our customer service, over the 20 years of experience we
                        have,
                        to be able to offer our customers the high-quality services they are looking for. We’ve always
                        championed individually, but right now, we want to transmit our professionalism to you. Having
                        said
                        that, our customer service is the magnet that will keep you close to us and will let you forget
                        about dawdling by other real estate companies. You will see it yourself, once a SAMA Emirates Properties
                        customer,
                        always a SAMA Emirates Properties customer
                    @else
                        بمجرد اتخاذك القرار بالتعامل معنا، فإننا نعدك ببذل قصارى جهدنا لضمان راحتك! قنحن نقدم الخدمات
                        العقارية الأكثر تميزًا ونحرص أن تكون عملية التواصل على أعلى مستوى من الفاعلية، ولهذا فقد واصلنا
                        تطوير خدمة العملاء لدينا على مدى السنوات العشرين التي تواجدنا خلالها في سوق العمل.
                        ضع ثقتك بنا ونعدك بأنك ستختبر تجربة تختلف كل الاختلاف عن التجارب المريرة والبطيئة التي يقدمها
                        غيرنا، فخدمة عملائنا ستكون نقطة التحول التي ستجعلك      تعود لشركتنا مرارًا وتكرارًا!"
                    @endif
                </div>
                <div class="p-lg-5 text-left">
                    @if(app()->getLocale()=="en")
                        <h1 style="color: rgb(3, 77, 138);">Timing</h1>
                        <div style="line-height: 8px;padding: 1.25rem;">
                            <p> We are at your service from <b class="color_black">8 am to 5 pm.</b></p>
                            <p> We’ll get back to you within <b class="color_black">24 hours.</b></p>
                        </div>
                    @else
                        <h1 style="color: rgb(3, 77, 138);">التوقيت</h1>
                        <div style="line-height: 8px;padding: 1.25rem;">
                            <p> نحن في خدمتكم من الساعة <b class="color_black"> الثامنة صباحًا حتى الخامسة مساءً.</b>
                            </p>
                            <p>سنعاود الاتّصال بكم خلال مدة أقصاها<b class="color_black"> 24 ساعة.</b></p>
                            @endif
                        </div>
                </div>
            </div>
        </div>
    </section>
    <section class="container-bg">
        <div class="container">
            <div class="row">
                <div class="col-md-7">
                    <div class="contact-main-custom">
                        @if(app()->getLocale()=="en")
                            <h1>{{ theme_option('about-us') }}</h1>
                        @else
                            <h1>{{ "العنوان" }}</h1>
                        @endif
                        <br>
                        <div class="container line-height-contact-us">
                            <div class="contact-name"
                                 style="text-transform: uppercase">{{ theme_option('company_name') }}</div>
                            <div class="contact-phone">{{ __('Phone') }}:  <span class="mobile">{{ theme_option('hotline') }} </span></div>
                            <div class="contact-email">{{ __('Email') }}: {{ theme_option('email') }}</div>
                            <div class="contact-address">{{ __('Address') }}: {{ theme_option('address') }}</div>
                        </div>
                    </div>
                </div>
                <div class="col-md-5">
                    <p class="color_white text-center">{{__("Click to open in Google Maps")}}</p>
                    <iframe
                        src="https://maps.google.com/maps?q=F9QV+2R9 - Marina Square - Hazza ' Bin Zayed The First St - Abu Dhabi&amp;t=&amp;z=13&amp;ie=UTF8&amp;iwloc=&amp;output=embed"
                        frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
                </div>
            </div>
        </div>
    </section>
    <section>
        <div class="container">
            <div class="row">
                @if(app()->getLocale()=="en")
                <div class="p-lg-5 text-left">
                    <h1 style="color: rgb(3, 77, 138);">Only One Call Away</h1>
                    <div style="line-height: 1.5rem;padding: 1.25rem;">
                        <p>With SAMA Emirates Properties, customers are always happy, satisfied, and they even recommend us to other
                            potential customers. The reason is quite simple; we have never failed our customers and we
                            have always offered them the best real estate services. We do believe that trust is earned,
                            that is why we do believe that working with us will never disappoint you. With SAMA Emirates Properties,
                            real estate is smooth sailing. The wait is over, reach out to us as soon as possible and get
                            to benefit from our exceptional services, our great communication skills, and our real
                            estate experts who will help you in every way possible! Contact us now, and everything else
                            will fall into place. We are waiting for you.</p>
                        <p><b>Contact us now!</b></p>
                    </div>
                </div>
                @else
                    <div class="p-lg-5 text-left">
                        <h1 style="color: rgb(3, 77, 138);">ننتظركم بفارغ الصبر</h1>
                        <div style="line-height: 1.5rem;padding: 1.25rem;">
                            <p>السعادة والرضى أمران مضمونان في كافة تعاملاتكم مع شركة سما الإمارات العقارية، وهذا ما يجعل عملاءنا يرشحون اسم شركتنا لكل من يبحث عن خدمات عقارية مضمونة، فلم يحدث أبدا خلال العشرين سنة التي مرت على تواجدنا في هذا القطاع أن خيبنا أمل عميل اختار التعامل معنا… معنا يصبح الصعب سهلًا، تواصل الآن لتستفيد من خدماتٍ لا تضاهى في سوق العقارات.</p>
                            <p><b> تواصلوا معنا الآن!</b></p>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </section>
    <section>
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <img src="{{asset("/storage/Logo_contact_us.png")}}"
                         class="img-fluid hover-shadow" alt="Los Angeles Skyscrapers"/>
                </div>
                <div class="col-md-6">
                    <form action="{{ route('public.send.contact') }}" method="post" class="generic-form">
                        <div>
                            @csrf
                            {!! apply_filters('pre_contact_form', null) !!}

                            <div class="radio-contact-us">
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input input-contact-us" type="radio"
                                           name="address"
                                           id="inlineRadio1" value="Mr">
                                    <label class="form-check-label" for="inlineRadio1">{{__("Mr")}}</label>
                                </div>

                                <div class="form-check form-check-inline">
                                    <input class="form-check-input input-contact-us" type="radio"
                                           name="address"
                                           id="inlineRadio2" value="Mrs">
                                    <label class="form-check-label" for="inlineRadio2">{{__("Mrs")}}</label>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-2 col-form-label">{{__("Name")}}*</label>
                                <div class="col-sm-10">
                                    <input class="form-control input-contact-us" type="text" name="name"
                                           placeholder=" {{ __('Name') }} *" required="">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-2 col-form-label">{{__("Email")}}</label>
                                <div class="col-sm-10">
                                    <input class="form-control input-contact-us" type="text" name="email"
                                           placeholder=" {{ __('Email') }} *" required="">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputEmail3" class="col-md-2 col-sm-2 col-form-label">{{__("Phone")}}
                                    *</label>
                                <div class="col-md-2 col-sm-2 pr-0 pl-custom">
                                    <input type="text" name="phone_1" placeholder=" "
                                           class="form-control input-contact-us">
                                </div>
                                <div class="col-md-8 col-sm-8">
                                    <input type="text" name="phone_2" placeholder="{{__("Phone")}}"
                                           class="form-control input-contact-us">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-2 col-form-label">{{ __('Title') }}</label>
                                <div class="col-sm-10">
                                    <input class="form-control input-contact-us" type="text" name="subject"
                                           placeholder=" {{ __('Title') }}">
                                </div>

                            </div>
                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-2 col-form-label">{{ __('Message') }}*</label>
                                <div class="col-sm-10">
                                     <textarea class="form-control input-contact-us" name="content" rows="6"
                                               minlength="10"
                                               placeholder=" {{ __('Message') }} *"
                                               required=""></textarea>
                                </div>
                            </div>
                            @if (is_plugin_active('captcha'))
                                @if (setting('enable_captcha'))
                                    <div class="form-group">
                                        {!! Captcha::display() !!}
                                    </div>
                                @endif

                                @if (setting('enable_math_captcha_for_contact_form', 0))
                                    <div class="form-group">
                                        {!! app('math-captcha')->input(['class' => 'form-control', 'id' => 'math-group', 'placeholder' => app('math-captcha')->label()]) !!}
                                    </div>
                                @endif
                            @endif

                            {!! apply_filters('after_contact_form', null) !!}

                            <div class="alert alert-success text-success text-left" style="display: none;">
                                <span></span>
                            </div>
                            <div class="alert alert-danger text-danger text-left" style="display: none;">
                                <span></span>
                            </div>
                            <br>
                            <div class="form-actions form-actions-custom">
                                <button class="btn-special btn-special-custom" type="submit">{{ __('Submit') }}</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
</div>
