@php
    $countries=\Botble\Location\Models\Country::where("status","published")->get();
    $cities=\Botble\Location\Models\City::where("country_id",1)->where("status","published")->get(); // get UAE
    $features=\Botble\RealEstate\Models\Feature::where("status","published")->get();
    $categories_form1 = get_property_categories(['indent' => '↳', 'conditions' => ['status' => \Botble\Base\Enums\BaseStatusEnum::PUBLISHED]]);
@endphp


<div class="modal filter_modal" id="filter_modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">

        <div class="modal-content">
            <div class="formm">
                <div class="    modal-footer justify-content-center" style="display: block !important;padding: 2rem !important;">
                    <div class="form-group" style="margin-bottom: 0% !important;">
                        <div class="row">
                            <div class="col-md-1">
                                <label class="homeSearchBoxText-area">
                                    {{__('I want a')}}
                                </label>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <div class="select--arrow">
                                        <select name="category_id" id="select-category" class="form-control">
                                            <option value="-1">{{ __('Select') }}</option>
                                            @foreach($categories_form1 as $category)
                                                <option value="{{ $category->id }}"   @if (request()->input('category_id') == $category->id) selected @endif>{{ $category->indent_text . ' ' . $category->name }}</option>
                                            @endforeach
                                        </select>
                                        <i class="fas fa-angle-down"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-1" style="max-width: 2% !important;">
                                <div class="homeSearchBoxText-area" style="margin-top: 10px;">
                                    {{__('for')}}
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <div class="select--arrow">
                                        <select id="type" name="type" class="form-control">
                                            <option value="">{{ __('Select') }}</option>
                                            <option @if (request()->path() == "rent-properties") selected @endif  value="rent">{{__('Rent')}}</option>
                                            <option @if (request()->path() == "buy-properties") selected @endif value="sale">{{__('Sale')}}</option>
                                        </select>
                                        <i class="fas fa-angle-down"></i>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-2">
                                <label class="location-area">
                                   {{ __("in") }} {{ __($_GET["location"] )}}
                                </label>
                            </div>

                            <div class="col-md-4 pr-0 pl-0">
                                <button  id="advanced_filter" type="submit" class="btn btn-primary advanced_filter-custom"><i class="fa fa-search"></i>
                                    {{__("Search Properties")}}
                                </button>
                                <button  type="button" onclick="displayAdvancedFilterArea()" class="btn btn-outline-primary"><i
                                        class="fa fa-sliders-h advanced_filter-custom-2"></i>
                                    {{__("Advanced Filter")}}
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal filter_modal" id="advanced-filter-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title pl-3">
                    {{__("please select all of your interests")}}
                </h5>
                <div>
                    <button onclick="closeAdvancedFilterArea()" type="button" id="close-custom" class="close"
                            data-dismiss="modal">&times;
                    </button>
                </div>
            </div>
            <div class="formm">
                <div class="modal-body justify-content-around">
                    <div class="col-3">
                        <div class="form-group">
                            <label for="property_type" class="filter-label">
                                {{ __('Property Type') }}
                            </label>
                            @if($categories_form1)
                                <select name="category_id" class="form-control" id="property-custom_2">
                                    <option value="-1">{{__("-- Select --")}}</option>
                                    @foreach($categories_form1 as $catg)
                                        <option value="{{$catg->id}}">{{$catg->name}}</option>
                                    @endforeach
                                </select>
                            @endif
                        </div>

                        <div class="form-group">
                            <label for="country" class="filter-label">
                                {{ __('Country') }}
                            </label>
                            @if($countries)
                                <select class="form-control">
                                    @php
                                        $cityData=\Botble\Location\Models\City::where('name', 'like', '%'.$_GET['location'].'%')->get()->first();
                                        if(isset($cityData->country_id) && !empty($cityData->country_id))
                                           $countryData=\Botble\Location\Models\Country::find($cityData->country_id)
                                    @endphp
                                    @if(isset($countryData->id) && !empty($countryData->id))
                                        <option value="{{$countryData->id}}"
                                                selected>{{__($countryData->name)}}</option>
                                    @endif
                                    {{--                                    @foreach($countries as $cou)--}}
                                    {{--                                        <option value="{{$cou->id}}">{{ $cou->name }}</option>--}}
                                    {{--                                    @endforeach--}}
                                </select>
                            @endif
                        </div>

                        <div class="form-group">
                            <label for="city" class="filter-label">
                                {{ __('City') }}
                            </label>
                            @if($cities)
                                <select name="location"   class="form-control" id="location_id_2">
                                    <option value="{{$_GET["location"]}}" selected>{{__($_GET["location"])}}</option>
{{--                                    @foreach($cities as $c)--}}
{{--                                        <option value="{{$c->name}}">{{$c->name}} , UAE</option>--}}
{{--                                    @endforeach--}}
                                </select>
                            @endif
                        </div>

                        <div class="form-group">
                            <label for="furnishing" class="filter-label">
                                {{ __('Furnishing') }}
                            </label>
                            <select name="is_furnished" class="form-control">
                                <option value="">{{__("-- Select --")}}</option>
                                <option value="1">{{__("Furnished")}}</option>
                                <option value="2">{{__("Not furnished")}}</option>
                                <option value="3">{{__("Partly furnished")}}</option>
                            </select>
                        </div>

{{--                        <div class="form-group">--}}
{{--                            <label for="completion" class="filter-label">--}}
{{--                                {{ __('Completion') }}--}}
{{--                            </label>--}}
{{--                            <select name="property_status" class="form-control">--}}
{{--                                <option value="">{{__("-- Select --")}}</option>--}}
{{--                                <option value="1">{{__("Off-plan")}}</option>--}}
{{--                                <option value="2">{{__("Launching Soon")}}</option>--}}
{{--                                <option value="3">{{__("Under Construction")}}</option>--}}
{{--                                <option value="4">{{__("Completed")}}</option>--}}
{{--                            </select>--}}
{{--                        </div>--}}
                    </div>

                    <div class="col-4">
                        <div class="form-group py-2">
                            <div class="mb-3">
                                <label class="filter-label" for="price">
                                    {{__("Price Range")}}
                                </label>
                                <div style="display: inline" id="currency">
                                    <input type="radio" id="feature"
                                           autocomplete="off" checked="checked" hidden="hidden"
                                           class="btn-check checked-btn" style="background-color: red;">
                                    <label class="btn checked-feature">{{__("AED")}}</label>
                                </div>
                            </div>

                            <input type="text" id="slider-js" class="js-range-slider" name="min_price"
                                   value="" data-min="0" data-max="50" data-from="0" data-to="0" data-postfix="M"/>

                        </div>
                        <div class="form-group ">

                            <div data-toggle="counter" class="qty d-flex align-items-center filter-label">
                                <i class="fa fa-bed mr-2" style="color: #00a8bf;"></i>
                                {{__("ADD BEDS")}} (<input id="input-counter-1" type="text" name="bedroom" value="0">)
                                <div class="minus mr-3 ml-3 minus-1">-</div>
                                <div class="plus plus-1">+</div>
                            </div>

                        </div>
                        <div class="form-group">

                            <div data-toggle="counter" class="qty d-flex align-items-center filter-label">
                                <i class="fas fa-shower propertie-details-icon mr-2" style="color: #00a8bf;"></i>
                                {{__("ADD BATH")}} (<input id="input-counter-2" type="text" name="bathroom" value="0">)
                                <div class="minus mr-3 ml-3 minus-2">-</div>
                                <div class="plus plus-2">+</div>
                            </div>
                        </div>
                        <div class="form-group pt-2">
                            <label class="filter-label" for="Footage">
                                {{__("Square Footage")}}
                            </label>
                            <input type="text" class="js-range-slider" name="min_square" value=""
                                   data-min="0"
                                   data-max="5000"
                                   data-from="0"
                                   data-to="0"
                            />
                        </div>
                    </div>

                    <div class="col-4">

                        <div class="form-group">
                            <label class="filter-label" for="features">
                                {{__("Property Features")}}
                            </label>

                            <div id="features">
                                @foreach($features as $f)
                                    <input type="checkbox" class="btn-check {{$f->id}}" name="features[{{$f->id}}]"
                                           value="0" id="feature" autocomplete="off" hidden>
                                    <label class="btn unchecked-feature" id="{{$f->id}}"
                                           for="feature">{{$f->name}}</label>
                                @endforeach
                            </div>
                        </div>
                        <div class="modal-footer justify-content-start">
                            <div class="">

                                {{--                                <p class="filter-label">--}}
                                {{--                                    <span>0</span> matching apartments--}}
                                {{--                                </p>--}}
                                <button type="submit" class="btn btn-primary pl-5 pr-5 pt-2 w-100">
                                    <i class="fa fa-sliders-h mr-2"></i>
                                    {{__("See Results")}}
                                </button>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
