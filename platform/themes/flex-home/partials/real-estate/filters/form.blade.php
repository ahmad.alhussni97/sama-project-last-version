@php
    $countries=\Botble\Location\Models\Country::where("status","published")->get();
    $cities=\Botble\Location\Models\City::where("country_id",1)->where("status","published")->get(); // get UAE
    $categories=\Botble\RealEstate\Models\Category::where("status","published")->get();
    $features=\Botble\RealEstate\Models\Feature::where("status","published")->get();
@endphp

<div class="modal filter_modal" id="filter_modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">

        <div class="modal-content modal-content-custom" style="padding-top: 10px !important;">
            <div class="modal-header justify-content-around">
                <div class="col-4">
                    <h5 class="modal-title">
                        <i class="fa fa-map-marker"></i>
                        Location
                    </h5>

                </div>
                <div class="col-4">
                    <h5 class="modal-title">
                        <i class="fa fa-sm fa-home"></i>
                        Property Type
                    </h5>
                </div>
                <div class="col-4">
                    <h5 class="modal-title">
                        <i class="fa fa-sm fa-dollar-sign"></i>
                        Average Price
                    </h5>
                </div>
            </div>
            <div class="formm">
                <div class="modal-body justify-content-around">
                    <div class="col-4">
                        <div class="form-group--inline justify-content-center">
                            <div class="form-group">
                                <div class="form-control-filter">
                                    @if($cities)
                                        <select name="location" id="location_id">
                                            <option value="-1">-- Select --</option>
                                            @foreach($cities as $c)
                                                <option value="{{$c->name}}">{{$c->name}} , UAE</option>
                                            @endforeach
                                        </select>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="form-group--inline justify-content-center display-block">
                            <div class="form-group">
                                <div class="form-control-filter">
                                    @if($categories)
                                        <select name="category_id" id="property-custom">
                                            <option value="-1">-- Select --</option>
                                            @foreach($categories as $catg)
                                                <option value="{{$catg->id}}">{{$catg->name}}</option>
                                            @endforeach
                                        </select>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-4 w-100">
                        <div class="form-group">
                            <div class="form-control-filter" id="price-filter-custom">
                                <input name="min_price" value="" style="width:40%"/>
                                <span style="color: grey;font-size: 20px;">&dash;</span>
                                <input name="max_price" value="" style="width:40%"/>
                            </div>

                        </div>
                    </div>
                </div>
                <div>
                    <div class="modal-footer justify-content-center modal-footer-custom">
                        <button id="advanced_filter" type="submit" class="btn btn-primary advanced_filter-custom">
                            <i class="fa fa-search"></i>
                            Search Properties
                        </button>
                    </div>
                    <div class="modal-footer justify-content-end modal-footer-custom">
                        <button type="button" class="btn btn-outline-primary" onclick="displayAdvancedFilter()">
                            <i class="fa fa-sliders-h"></i>
                            Advanced Filter
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal filter_modal" id="advanced-filter-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title pl-3">
                    please select all of your interests
                </h5>
                <div>
                    <button onclick="closeAdvancedFilter()" type="button" id="close-custom" class="close"
                            data-dismiss="modal">&times;
                    </button>
                </div>
            </div>
            <div class="formm">
                <div class="modal-body justify-content-around">
                    <div class="col-3">
                        <div class="form-group">
                            <label for="property_type" class="filter-label">
                                {{ __('Property Type') }}
                            </label>
                            @if($categories)
                                <select name="category_id" class="form-control" id="property-custom_2">
                                    <option value="-1">-- Select --</option>
                                    @foreach($categories as $catg)
                                        <option value="{{$catg->id}}">{{$catg->name}}</option>
                                    @endforeach
                                </select>
                            @endif
                        </div>

                        <div class="form-group">
                            <label for="country" class="filter-label">
                                {{ __('Country') }}
                            </label>
                            @if($countries)
                                <select name="country" class="form-control">
                                    <option value="">-- Select --</option>
                                    @foreach($countries as $cou)
                                        <option value="{{$cou->id}}">{{ $cou->name }}</option>
                                    @endforeach
                                </select>
                            @endif
                        </div>

                        <div class="form-group">
                            <label for="city" class="filter-label">
                                {{ __('City') }}
                            </label>
                            @if($cities)
                                <select name="location" class="form-control" id="location_id_2">
                                    <option value="-1">-- Select --</option>
                                    @foreach($cities as $c)
                                        <option value="{{$c->name}}">{{$c->name}} , UAE</option>
                                    @endforeach
                                </select>
                            @endif
                        </div>

                        <div class="form-group">
                            <label for="furnishing" class="filter-label">
                                {{ __('furnishing') }}
                            </label>
                            <select name="is_furnished" class="form-control">
                                <option value="">-- Select --</option>
                                <option value="1">Furnished</option>
                                <option value="2">Not furnished</option>
                                <option value="3">Partly furnished</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="completion" class="filter-label">
                                {{ __('Completion') }}
                            </label>
                            <select name="property_status" class="form-control">
                                <option value="">-- Select --</option>
                                <option value="1">Off-plan</option>
                                <option value="2">Launching Soon</option>
                                <option value="3">Under Construction</option>
                                <option value="4">Completed</option>
                            </select>
                        </div>
                    </div>

                    <div class="col-4">
                        <div class="form-group py-2">
                            <div class="mb-3">
                                <label class="filter-label" for="price">
                                    Price Range
                                </label>
                                <div style="display: inline" id="currency">
                                    <input type="radio" id="feature"
                                           autocomplete="off" checked="checked" hidden="hidden"
                                           class="btn-check checked-btn" style="background-color: red;">
                                    <label class="btn checked-feature">AED</label>
                                </div>
                            </div>

                            <input type="text" class="js-range-slider" name="min_price"
                                   value="" data-min="0" data-max="50" data-from="0" data-to="0" data-postfix="M"/>
                        </div>
                        <div class="form-group ">

                            <div data-toggle="counter" class="qty d-flex align-items-center filter-label">
                                <i class="fa fa-bed mr-2" style="color: #00a8bf;"></i>
                                ADD BEDS (<input id="input-counter-1" type="text" name="bedroom" value="0">)
                                <div class="minus mr-3 ml-3 minus-1">-</div>
                                <div class="plus plus-1">+</div>
                            </div>

                        </div>
                        <div class="form-group">

                            <div data-toggle="counter" class="qty d-flex align-items-center filter-label">
                                <i class="fa fa-bed mr-2" style="color: #00a8bf;"></i>
                                ADD BATH (<input id="input-counter-2" type="text" name="bathroom" value="0">)
                                <div class="minus mr-3 ml-3 minus-2">-</div>
                                <div class="plus plus-2">+</div>
                            </div>
                        </div>
                        <div class="form-group pt-2">
                            <label class="filter-label" for="Footage">
                                Square Footage
                            </label>
                            <input type="text" class="js-range-slider" name="min_square" value=""
                                   data-min="0"
                                   data-max="5000"
                                   data-from="0"
                                   data-to="0"
                            />
                        </div>
                    </div>

                    <div class="col-4">
                        <div class="form-group">
                            <label class="filter-label" for="features">
                                Property Features
                            </label>

                            <div id="features">
                                @foreach($features as $f)
                                    <input type="checkbox" class="btn-check {{$f->id}}" name="features[{{$f->id}}]"
                                           value="0" id="feature" autocomplete="off" hidden>
                                    <label class="btn unchecked-feature" id="{{$f->id}}"
                                           for="feature">{{$f->name}}</label>
                                @endforeach
                            </div>
                        </div>
                        <div class="modal-footer justify-content-start">
                            <div class="">

                                <p class="filter-label">
                                    <span>0</span> matching apartments
                                </p>
                                <button type="submit" class="btn btn-primary pl-5 pr-5 pt-2 w-100">
                                    <i class="fa fa-sliders-h mr-2"></i>
                                    See Results
                                </button>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>



