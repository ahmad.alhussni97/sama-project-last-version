<div id="loading">
    <div class="half-circle-spinner">
        <div class="circle circle-1"></div>
        <div class="circle circle-2"></div>
    </div>
</div>

<input type="hidden" name="page" data-value="{{ $projects->currentPage() }}">
@if ($projects->count())
    <div class="row">
        @foreach ($projects as $project)
            <div class="card-outline">
                {!! Theme::partial('real-estate.projects.item', compact('project')) !!}
            </div>
        @endforeach
    </div>
@else
    <div class="alert alert-warning" role="alert">
        {{ __('0 results') }}
    </div>
@endif


{{--<div class="colm10 col-sm-12">--}}
{{--    <nav class="d-flex justify-content-center pt-3">--}}
{{--        <ul id="pagination-project" class="pagination pagination-sm border-page">--}}
{{--            {!! $projects->links() !!}--}}
{{--        </ul>--}}
{{--    </nav>--}}
{{--</div>--}}

<div class="col-sm-12">
    <nav class="d-flex justify-content-center pt-3" aria-label="Page navigation example">
        {!! $projects->withQueryString()->onEachSide(1)->links() !!}
    </nav>
</div>
