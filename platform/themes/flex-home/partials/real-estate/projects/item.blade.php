@php
    if(app()->getLocale() =="ar"){

       $staticUrlInner="/ar/projects/";

       if( $project->status == 'Not available')
          $project->status=__('Not available');

       if($project->status == "Under Construction")
          $project->status=__('Under Construction');

       if($project->status == "Completed")
           $project->status=__('Completed');

       if($project->status == "Launching Soon")
           $project->status=__('Launching Soon');

       if($project->status == "Off-plan")
            $project->status=__('Off-plan');

       if($project->status == "Sold")
           $project->status=__('Sold');

} else
   $staticUrlInner="/projects/";
@endphp

<div class="card mb-3 main-card-project">
        <div class="row no-gutters">
            <div class="col-md-3">
                <div>
                    <a href="{{url($staticUrlInner.$project->slug)}}">
                        @php
                           $ImgProject=RvMedia::getImageUrl($project->image,'', false, RvMedia::getDefaultImage());
                            if (!isset($ImgProject) || empty($ImgProject))
                                $ImgProject=RvMedia::getImageUrl($project->image,'thumb', false, RvMedia::getDefaultImage());
                         @endphp
                        <img
                            src="{{$ImgProject}} "
                            class="card-img" alt="...">
                    </a>
                </div>
            </div>

            <div class="col-md-9">
                <div class="card-body row card-body-arabic">
                    <div class="card-text col-md-7 card-title-project">
                        <a class="project-hover" href="{{url($staticUrlInner.$project->slug)}}">
                            <p>
                                <b class="color_black">{{$project->name}}</b>
                            </p>
                        </a>
                        <a class="project-hover" href="{{url($staticUrlInner.$project->slug)}}">
                            <p class="card-title  card-body-project">

                                <span
                                    class="color_black">{{__( $project->city()->first()->name ) }}  {{$project->status}} </span>
                                <br><span>{{__("Starting Price")}} {{ $project->price_from}}</span>
                            </p>
                        </a>
                    </div>
                    <div class="col-md-5">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="background-project-icon">
                                    @if($project->email)
                                        <a href="mailto:{{$project->email}}">
                                                         <span class="icon-project">
                                                            <i class="pr fas fa-envelope"></i>
                                                        </span>
                                        </a>
                                    @endif
                                    @if($project->phone)
                                        <a href="tel:{{$project->phone}}">
                                                 <span class="icon-project">
                                                    <i class="pr fas fa-phone"></i>
                                                   </span>
                                        </a>
                                    @endif
                                    @if($project->slug)
                                        <a href="#" rel="{{url($staticUrlInner.$project->slug)}}"
                                           class="shareButton">
                                                    <span class="icon-project">
                                                        <i class="pr fas fa-share-alt"></i>
                                                    </span>
                                        </a>
                                    @endif

                                </div>
                            </div>
                            @if($project->slug)
                                <div class="col-md-6 remove-hover-from-button">
                                    <a href="{{url($staticUrlInner.$project->slug)}}"
                                       class="button-project">{{__("View Project")}}<i class='fas fa-arrow-right'></i>
                                    </a>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

