<div class="boxsliderdetail">
    @if(!str_contains(url()->current(),"/projects/"))
        <div class="slidetop">
            <div class="carousel-inner-project">
                @foreach ($object->images as $image)
                    <div class="item" data-src="{{ RvMedia::getImageUrl($image, null, false, RvMedia::getDefaultImage()) }}">
                        <img src="{{ RvMedia::getImageUrl($image, null, false, RvMedia::getDefaultImage()) }}"
                             class="showfullimg"
                             rel="{{ $loop->index }}"
                             alt="{{ $object->name }}"
                             title="{{ $object->name }}">
                    </div>
                @endforeach
            </div>
        </div>
    @endif
    <div class="slidebot">
        <div class="row">
            @if(!str_contains(url()->current(),"/projects/"))
                @if(sizeof($object->images) > 1)
                @php $counter=0   @endphp
                    <div class="row mx-auto my-auto  justify-center-images">
                        <div id="recipeCarousel" class="carousel carousel-project slide"
                             data-ride="carousel">
                            <div class="carousel-inner carousel-inner-project" role="listbox">
                                @foreach($object->images as $image)
                                    <li class="property-img carousel-item  @if($counter==0)   active @endif" data-src="{{ RvMedia::getImageUrl($image, null, false, RvMedia::getDefaultImage()) }}" >
                                        <div class="col-md-2">
                                            <div class="item cthumb">
                                                <img
                                                    src="{{ RvMedia::getImageUrl($image, null, false, RvMedia::getDefaultImage()) }}"
                                                    class="showfullimg">
                                            </div>
                                        </div>
                                    </li>
                                    @php $counter++; @endphp
                                @endforeach
                            </div>
                            <a class="carousel-control-prev bg-dark w-auto" href="#recipeCarousel"
                               role="button" data-slide="prev">
                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="carousel-control-next bg-dark w-auto" href="#recipeCarousel"
                               role="button" data-slide="next">
                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>
                    </div>

                @endif
            @endif
            <div class="col-6 col-md-5 col-sm-6" style="align-self: center;display:none;">
                <div class="row control justify-content-sm-end justify-content-center">

                    @if ($object->video_url)
                        <div class="col-6 col-sm-4 col-md-4 col-lg-2 itemct px-1 popup-youtube"
                             href="{{ $object->video_url }}">
                            <div class="icon">
                                <i class="fab fa-youtube"></i>
                                <p>{{ __('Youtube') }}</p>
                            </div>
                        </div>
                    @endif
                    <div class="col-sm-4 col-md-4 col-lg-2 itemct d-none d-sm-block px-1 show-gallery-image">
                        <div class="icon">
                            <i class="fas fa-th"></i>
                            <p>{{ __('Gallery') }}</p>
                        </div>
                    </div>
                    @if ($object->latitude && $object->longitude)
                        <div class="col-6 col-sm-4 col-md-4 col-lg-2 itemct px-1"
                             data-magnific-popup="#trafficMap"
                             data-map-id="trafficMap"
                             data-popup-id="#traffic-popup-map-template"
                             data-map-icon="{{ $object->map_icon }}"
                             data-center="{{ json_encode([$object->latitude, $object->longitude]) }}">
                            <div class="icon">
                                <i class="far fa-map"></i>
                                <p>{{ __('Map') }}</p>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
