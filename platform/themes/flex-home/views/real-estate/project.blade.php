@php
    Theme::asset()->usePath()->add('leaflet-css', 'libraries/leaflet.css');
    Theme::asset()->container('footer')->usePath()->add('leaflet-js', 'libraries/leaflet.js');
    Theme::asset()->usePath()->add('magnific-css', 'libraries/magnific/magnific-popup.css');
    Theme::asset()->container('footer')->usePath()->add('magnific-js', 'libraries/magnific/jquery.magnific-popup.min.js');

$otherProjects=\Botble\RealEstate\Models\Project::all()->random(8);
$dataPerson=\Botble\RealEstate\Models\Project::find($project->id);
$investor=\Botble\RealEstate\Models\Investor::find($dataPerson->investor_id);

if(isset($investor->name) && !empty($investor->name)){
   $nameAccountant=substr($investor->name,0,strpos($investor->name,' '));
   $accountant=\Botble\RealEstate\Models\Account::where("first_name",'LIKE', $nameAccountant. '%')->get()->first();
}

$folderName = "storage";
function getٍSingleProjectImageSrc($folderName, $img)
{

    $result = null;
    $j = 0;
    $size = ['big', 'medium', 'small', 'thumb'];

    while ($j < count($size)) {

        $result = RvMedia::getImageUrl($img, $size[$j], false, RvMedia::getDefaultImage());
        $imageName = basename($result);

        if (file_exists($folderName . "/" . $imageName)) return $result;
        $j++;
    }

    return $result;
}

@endphp
<main class="detailproject mt-5">
    @include(Theme::getThemeNamespace() . '::views.real-estate.includes.slider', ['object' => $project])
    <div class="bgarea hidden-xs bgarea-project">
        <div class="row">
            <div class="col-md-7">
                <div  class="description description-custom description-responsive <?php if (isset($accountant) && !empty($accountant)) echo 'margin-title-project'  ?>">
                    {!! Theme::partial('breadcrumb') !!}
                </div>
            </div>
            @if (isset($accountant) && !empty($accountant))
                <div class="col-md-4">
                    <div class="container-fluid">
                        <div class="row profile_image"
                             style="height: 294px;background: #e1e6ec;/*height: 300px;*/border-top-left-radius: 20px;border-top-right-radius: 20px;border-top-left-radius: 20px;">
                            <div class="col" style="padding:0px;z-index:9;height: 100%;">
                                <div>
                                    @if ($accountant->username)
                                        <a class="profile-top"  href="{{ route('public.agent', $accountant->username) }}"
                                           style="position: relative;top: 80px;width: 80%;margin: 0px auto;background: rgb(240, 247, 255);border-radius: 20px;box-shadow: rgb(99 99 99 / 53%) 0px 0px 4px;display: flex">
                                            @if ($accountant->avatar->url)
                                                <img src="{{ RvMedia::getImageUrl($accountant->avatar->url, 'thumb') }}"
                                                     alt="{{ $accountant->name }}"
                                                     class="img-thumbnail-custom">
                                            @else
                                                <img src="/storage/layer1.svg" alt="{{ $accountant->name }}"
                                                     class="img-thumbnail-custom">
                                            @endif
                                        </a>
                                    @endif
                                </div>
                            </div>
                            <div class="col">
                                <div class="row">
                                    <div class="col padding-profile" style="padding-top: 83px;padding-left: 12px;">
                                        <h1 style="font-size: 30px;" class="font-size-15-custom font-size-15-custom-project">
                                            @if ($accountant->username)
                                                <a href="{{ route('public.agent', $accountant->username) }}">{{__('SAMA Emirates Properties')}}
                                                    - {{ $accountant->name }}</a>
                                            @else
                                                {{ $accountant->name }}
                                            @endif
                                        </h1>
                                        <p style="margin-bottom: 37px;" class="font10 font10-project">
                                            @php
                                                if ($accountant->phone) {
                                                   Theme::set('hotlineNumber', $accountant->phone);
                                                }
                                            @endphp
                                            <a class="mobile color_basic"
                                               href="tel:{{ $accountant->phone }}">{{ $accountant->phone ?: theme_option('hotline') }}</a>
                                            <br><a class="color_basic"
                                                   href="mailto:{{ $accountant->email }}"> {{ $accountant->email }} </a>
                                            <br>@if($accountant->is_verified == 0 ){{__('Not verified')}} @else {{__('Verified')}} @endif
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row"
                             style="background: #034d8a;border-bottom-right-radius: 20px;border-bottom-left-radius: 20px;">
                            <div class="col" style="text-align: center;padding: 20px;">
                                <a href="#" style="color: rgb(255,255,255);">
                                    @if ($accountant->username)
                                        <a class="font-size-14-custom view-full-profile"
                                           href="{{ route('public.agent', $accountant->username) }}"
                                           style="color: #fff;">{{ __('View Full Profile') }}</a>
                                    @endif
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </div>

    <div class="container-fluid bgmenupro bgmenupro-project">
        <div class="container-fluid w90 padtop30" style="padding: 15px 0;">
            <div class="col-12">
                <h1 class="title"
                    style="font-size: 1.5rem; font-weight: bold; margin-bottom: 0;">{{ $project->name }}</h1>
                <p class="addresshouse">
                    <i class="fas fa-map-marker-alt"></i> {{ $project->city->name }}, {{ $project->city->state->name }}
                    @if (setting('real_estate_display_views_count_in_detail_page', 0) == 1)
                        <span class="d-inline-block" style="margin-left: 10px"><i
                                class="fa fa-eye"></i> {{ $project->views }} {{ __('views') }}</span>
                    @endif
                    <span class="d-inline-block" style="margin-left: 10px"><i class="fa fa-calendar-alt"></i> {{ $project->created_at->translatedFormat('M d, Y') }}</span>
                </p>
            </div>
        </div>
    </div>

    <div class="container-fluid w90 padtop30 single-post">
        <section class="general">
            <div class="row">
                <div class="col-md-8">
                    <div class="head">{{ __('Overview') }}</div>
                    <span class="line_title"></span>
                    <div class="row">

                        <div class="col-sm-6 lineheight220">
                            <div><span>{{ __('Status_Project') }}:</span> <b>{{ __($project->status->label()) }}</b>
                            </div>
                            @if ($project->categories()->count())
                                <div><span>{{ __('Category') }}:</span>
                                    <strong>{{ implode(', ', array_unique($project->categories()->pluck('name')->all())) }}</strong>
                                </div> @endif
                            @if ($project->investor->name)
                                <div><span>{{ __('Investor') }}:</span> <b>{{ $project->investor->name }}</b>
                                </div> @endif
                            @if ($project->price_from || $project->price_to)
                                <div>
                                    <span>{{ __('Price') }}:</span>
                                    <b>@if ($project->price_from)
                                            <span class="from">{{ __('From') }}</span>
                                            {{ format_price($project->price_from, $project->currency)  }} @endif
                                        <span class="from">{{ __('To') }}</span>
                                        @if ($project->price_to)
                                            - {{ format_price($project->price_to, $project->currency) }} @endif
                                    </b>
                                </div>
                            @endif
                        </div>
                        <div class="col-sm-6 lineheight220">
                            @if ($project->number_block)
                                <div><span>{{ __('Number of blocks') }}:</span>
                                    <b>{{ number_format($project->number_block) }}</b></div> @endif
                            @if ($project->number_floor)
                                <div><span>{{ __('Number of floors') }}:</span>
                                    <b>{{ number_format($project->number_floor) }}</b></div>@endif
                            @if ($project->number_flat)
                                <div><span>{{ __('Number of flats') }}:</span>
                                    <b>{{ number_format($project->number_flat) }}</b></div>@endif
                        </div>
                    </div>

                    <div class="head">{{ __('Description') }}</div>
                    @if ($project->content)
                        {!! BaseHelper::clean($project->content) !!}
                    @endif
                    @if ($project->features->count())
                        <div class="head">{{ __('Features') }}</div>
                        <div class="row">
                            @php $project->features->loadMissing('metadata'); @endphp
                            @foreach($project->features as $feature)
                                <div class="col-sm-4">
                                    @if ($feature->getMetaData('icon_image', true))
                                        <p><i><img src="{{ RvMedia::getImageUrl($feature->getMetaData('icon_image', true)) }}" alt="{{ $feature->name }}" style="vertical-align: top; margin-top: 3px;" width="18" height="18"></i> {{ $feature->name }}</p>
                                    @else
                                        <p><i class="@if ($feature->icon) {{ $feature->icon }} @else fas fa-check @endif text-orange text0i"></i>  {{ $feature->name }}</p>
                                    @endif
                                </div>
                            @endforeach
                        </div>
                    @endif
                    <br>
                    @if ($project->facilities->count())
                        <div class="row">
                            <div class="col-sm-12">
                                <h5 class="headifhouse">{{ __('Distance key between facilities') }}</h5>
                                <div class="row">
                                    @php $project->facilities->loadMissing('metadata'); @endphp
                                    @foreach($project->facilities as $facility)
                                        <div class="col-sm-4">
                                            @if ($facility->getMetaData('icon_image', true))
                                                <p>
                                                    <i><img src="{{ RvMedia::getImageUrl($facility->getMetaData('icon_image', true)) }}"
                                                            alt="{{ $facility->name }}"
                                                            style="vertical-align: top; margin-top: 3px;" width="18"
                                                            height="18"></i> {{ $facility->name }}
                                                    @if(isset($facility->pivot->distance) && !empty($facility->pivot->distance))
                                                        - {{ $facility->pivot->distance }}
                                                    @endif
                                                </p>
                                            @else
                                                <p>
                                                    <i class="@if ($facility->icon) {{ $facility->icon }} @else fas fa-check @endif text-orange text0i"></i> {{ $facility->name }}
                                                    @if(isset($facility->pivot->distance) && !empty($facility->pivot->distance))
                                                        - {{ $facility->pivot->distance }}
                                                    @endif
                                                </p>
                                            @endif
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    @endif
                    <br>
                    @php
                        if (isset($project->country_id) && !empty($project->country_id))
                               $countryData=\Botble\Location\Models\Country::find($project->country_id);
                            $country_name=$countryData->name??'';
                    @endphp
                    @if ($project->latitude && $project->longitude)
                        {!! Theme::partial('real-estate.elements.traffic-map-modal', ['location' => $project->location . ', ' . $country_name. ', ' . $project->city_name]) !!}
                    @else
                        {!! Theme::partial('real-estate.elements.gmap-canvas', ['location' => $project->location . ', ' . $country_name]) !!}
                    @endif
                    @if ($project->video_url)
                        {!! Theme::partial('real-estate.elements.video', ['object' => $project, 'title' => __('Project video')]) !!}
                    @endif
                    <br>
                    {!! Theme::partial('share', ['title' => __('Share this project'), 'description' => $project->description]) !!}
                    <div class="clearfix"></div>
                    <br>
                </div>
                <div class="col-md-4 padtop10">
                    <div class="boxright p-3">
                        {!! Theme::partial('consult-form', ['type' => 'project', 'data' => $project]) !!}
                    </div>
                </div>
            </div>


            <div>
                <h5 class="headifhouse">{{__("Project Images")}} </h5>
                @if(isset($project->images) && !empty($project->images))
                    <div class="projecthome mb-4">
                        <div class="row mb-2">
                            <div class="container-fluid text-center my-3">
                                <div class="row mx-auto my-auto  justify-center-images">
                                    <div id="recipeCarousel" class="carousel carousel-project slide"
                                         data-ride="carousel">
                                        <div class="carousel-inner carousel-inner-project" role="listbox">
                                            @php $counter=0; @endphp
                                            @foreach($project->images as $k=>$imgProj)
                                                <li class="carousel-item  @if($counter==0)   active @endif" data-src="{{getٍSingleProjectImageSrc($folderName,$imgProj)}}" >
                                                    <div class="col-md-2">
                                                        <div class="movie-card m-1">
                                                            <div class="movie-img">
                                                                <img
                                                                    src="{{getٍSingleProjectImageSrc($folderName,$imgProj)}}"
                                                                    class="img-fluid img-fluid-project">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                @php $counter++; @endphp
                                            @endforeach
                                        </div>
                                        <a class="carousel-control-prev bg-dark w-auto" href="#recipeCarousel"
                                           role="button" data-slide="prev">
                                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                            <span class="sr-only">Previous</span>
                                        </a>
                                        <a class="carousel-control-next bg-dark w-auto" href="#recipeCarousel"
                                           role="button" data-slide="next">
                                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                            <span class="sr-only">Next</span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
            </div>


            <div class="container-fluid">
                <div class="row">
                    @if(sizeof($project->floor_plans) > 0)
                        <div class="col">
                            <h5 class="headifhouse"> {{ __("Floor plans") }}</h5>
                            <div style="display: block !important;">
                                <div class="owl-carousel" id="listcarouselthumb">
                                    @foreach($project->floor_plans as $key=>$val)
                                        <div class="item cthumb" rel="{{ $key }}">
                                            <img
                                                src="{{ RvMedia::getImageUrl($val, null, false, RvMedia::getDefaultImage()) }}"
                                                class="showfullimg showfullimg-custom"
                                                rel="{{ $key }}"
                                                alt="{{ $val }}"
                                                data-mfp-src="{{ RvMedia::getImageUrl($val, null, false, RvMedia::getDefaultImage()) }}"
                                                title="{{ $val }}">
                                        </div>
                                    @endforeach
                                </div>
                                <i class="fas fa-chevron-right ar-next"></i>
                                <i class="fas fa-chevron-left ar-prev"></i>
                            </div>
                        </div>
                    @endif
                </div>
            </div>

            {{--            <h5  class="headifhouse">{{ __('Properties For Sale') }}</h5>--}}
            {{--            <div class="projecthome mb-2">--}}
            {{--                <property-component type="project-properties-for-sell" :project_id="{{ $project->id }}" url="{{ route('public.ajax.properties') }}" :show_empty_string="true"></property-component>--}}
            {{--            </div>--}}

            {{--                        <h5  class="headifhouse">{{ __('Properties For Rent') }}</h5>--}}
            {{--                        <div class="projecthome mb-4">--}}
            {{--                            <property-component type="project-properties-for-rent" :project_id="{{ $project->id }}" url="{{ route('public.ajax.properties') }}" :show_empty_string="true"></property-component>--}}
            {{--                        </div>--}}

            <div>
                <h5 class="headifhouse">{{__("Other Projects")}} </h5>
                @if(isset($otherProjects) && !empty($otherProjects))
                    <div class="projecthome mb-4">
                        <div class="row mb-2">
                            @foreach($otherProjects as $op)
                                @php
                                    if(app()->getLocale() =="ar")
                                          $urlData="/ar/projects/";
                                          else
                                          $urlData="/projects/";

                                @endphp
                                <div class="col-md-3">
                                    <a href="{{url($urlData.$op->slug)}}">
                                        @if(array_key_exists(array_key_first($op->images),$op->images))
                                            <img class="image-other-projecct"
                                                 src="{{getٍSingleProjectImageSrc($folderName,$op->images[array_key_first($op->images)])}}">
                                        @else
                                            <img class="image-other-projecct" src="{{url('storage/defult.png')}}">
                                        @endif
                                        <div class="mt-2">
                                            <span
                                                class="font-size-14 color_black font-size-project">{{$op->name}}</span>
                                        </div>
                                    </a>
                                </div>
                            @endforeach
                        </div>
                    </div>
                @endif
            </div>

        </section>
    </div>
</main>

<script id="traffic-popup-map-template" type="text/x-custom-template">
    {!! Theme::partial('real-estate.projects.map', ['project' => $project]) !!}
</script>



