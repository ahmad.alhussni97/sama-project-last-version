@php
$projectsFeatured = \Botble\RealEstate\Models\Project::where("is_featured", 1)->orderBy("id", "Desc")->get();
$city = \Botble\Location\Models\City::all();
$ctg = \DB::table("re_categories")->get();
$folderName = "storage";

function getProjectImageSrc($folderName, $img)
{

    $result = null;
    $j = 0;
    $size = ['big', 'medium', 'small', 'thumb'];

    while ($j < count($size)) {

        $result = RvMedia::getImageUrl($img, $size[$j], false, RvMedia::getDefaultImage());
        $imageName = basename($result);

        if (file_exists($folderName . "/" . $imageName)) return $result;
        $j++;
    }

    return $result;
}

if(app()->getLocale() =="ar")
    $staticUrl="/ar/projects/";
else
    $staticUrl="/projects/";

@endphp

<section class="main-homes" xmlns="http://www.w3.org/1999/html" xmlns="http://www.w3.org/1999/html">
    <div class="bgheadproject2 hidden-xs project-bg">
        <div class="description description-custom">
            {!! Theme::partial('breadcrumb') !!}
        </div>
        <div class="container container_project">
            <h1 class="text-left panel-sell-with-us">{{ SeoHelper::getTitle() }}</h1>
            <p class="text-left">{{ theme_option('properties_description') }}</p>
        </div>

        @if($projectsFeatured)
            <div class="container project-slide">
                <div>
                    <!--Carousel Wrapper-->
                    <div id="multi-item-example" class="carousel slide carousel-multi-item" data-ride="carousel">
                        <!--Slides-->
                        @php
                            $active=false;
                            $i=0;
                            $count=count($projectsFeatured);
                        @endphp
                        <div class="carousel-inner carousel-inner-custom" role="listbox">
                            @foreach($projectsFeatured as $p)

                                @if($i==0)
                                    <div class="carousel-item @if(!$active) active @php $active=true; @endphp @endif">
                                        <div class="row row-custom">
                                            <div class="col-md-4 col-md-4-slide" role="{{$i}}" count="{{$count}}"
                                                 id="{{$p->id}}">
                                                <div class="mb-2">
                                                    <a href="{{url($staticUrl.$p->slug)}}">
                                                        @if(array_key_exists(array_key_first($p->images),$p->images))
                                                            <img class="card-img-top"
                                                                 src="{{getProjectImageSrc($folderName,$p->images[array_key_first($p->images)])}}"
                                                                 alt="Card image cap">
                                                        @else
                                                            <img class="card-img-top"
                                                                 src="{{url('storage/defult.png')}}">
                                                        @endif
                                                        <div class="centered-slide">
                                                            <div class="project-slide-title">
                                                                   <span class="font-size-16"> {{__($p->city()->first()->name)}}, {{__("UAE")}} </span>
                                                                <br>
                                                                <span class="title-project">{{$p->name}}</span>
                                                            </div>
                                                            <span class="button-project-slide">{{__("View Project")}}<i
                                                                    class='fas fa-arrow-right'></i></span>
                                                        </div>
                                                    </a>
                                                </div>
                                            </div>
                                            @php $count--; @endphp
                                            @elseif($i==1 || $i==2)
                                                <div class="col-md-4 clearfix d-none d-md-block col-md-4-slide"
                                                     role="{{$i}}" count="{{$count}}" id="{{$p->id}}">
                                                    <div class="mb-2">
                                                        <a href="{{url($staticUrl.$p->slug)}}">
                                                            @if(array_key_exists(array_key_first($p->images),$p->images))
                                                                <img class="card-img-top"
                                                                     src="{{getProjectImageSrc($folderName,$p->images[array_key_first($p->images)])}}"
                                                                     alt="Card image cap">
                                                            @else
                                                                <img class="card-img-top"
                                                                     src="{{url('storage/defult.png')}}">
                                                            @endif
                                                            <div class="centered-slide">
                                                                <div class="project-slide-title">
                                                                    <span class="font-size-16"> {{__($p->city()->first()->name)}}, {{__("UAE")}} </span>
                                                                    <br>
                                                                    <span class="title-project">{{$p->name}}</span>
                                                                </div>
                                                                <span class="button-project-slide">{{__("View Project")}}<i
                                                                        class='fas fa-arrow-right'></i></span>
                                                            </div>
                                                        </a>
                                                    </div>
                                                </div>
                                                @php $count--; @endphp
                                            @endif
                                            @if(($i==2) || $count==0)
                                        </div>
                                    </div>
                                @endif
                                @php
                                    if($i >= 2)
                                    $i=0;
                                    else
                                    $i++;
                                @endphp
                            @endforeach
                        </div>

                        @if(count($projectsFeatured)  > 3)
{{--                            <div class="carousel-arrow">--}}
{{--                                <a class="btn-floating  arrow-slide-outline" href="#multi-item-example"--}}
{{--                                   data-slide="next">--}}
{{--                                    @if(str_contains(Route::current()->uri,"ar"))--}}
{{--                                        <span class="hide">التالي</span>--}}
{{--                                        <i class="fas fa-arrow-right arrow-slide fa-arrow-right-slide"></i>--}}
{{--                                    @else--}}
{{--                                        <i class="fas fa-arrow-right arrow-slide fa-arrow-right-slide"></i>--}}
{{--                                        <span class="hide">Next</span>--}}
{{--                                    @endif--}}
{{--                                </a>--}}
{{--                            </div>--}}
                        <div>
                            <a class="carousel-control-prev carousel-control-prev-custom" href="#multi-item-example" role="button" data-slide="prev">
                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="carousel-control-next carousel-control-next-custom" href="#multi-item-example" role="button" data-slide="next">
                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>
                        @endif

                        <ol class="carousel-indicators">
                            @for($i=0; $i < (count($projectsFeatured)/3) ;$i++ )
                                <li data-target="#multi-item-example" data-slide-to="{{$i}}"
                                    @if($i==0) class="active" @endif></li>
                            @endfor
                        </ol>
                        <!--/.Indicators-->
                    </div>
                    <!--/.Carousel Wrapper-->


                </div>
            </div>
        @endif

    </div>

    <script src="https://kit.fontawesome.com/a076d05399.js" crossorigin="anonymous"></script>
</section>

<section>
    <div class="big-title-projects">
        <p><span class="color_black font_weight_inherit"> {{__("All")}} </span> <span
                class="project-title">{{__("Projects")}}</span></p>
    </div>
    <form action="{{ url()->current() }}" method="get" id="ajax-filters-form">
        <div class="list-project">
            <div class="row justify-content-center">
                <div class="col-md-2">
                    <select name="country_id" class="form-control border-radius">
                        <option value="-1">{{__("Country")}} </option>
                        <option
                            value="1" @php if(isset($_GET["country"]) && $_GET["country"] == 1)  echo "selected" @endphp>
                            {{ __('United arab emirates') }}
                        </option>
                        <option
                            value="2" @php if(isset($_GET["country"]) && $_GET["country"] == 2)  echo "selected"  @endphp>
                            {{ __('Egypt') }}
                        </option>
                    </select>
                </div>
                @if($city)
                    <div class="col-md-2">
                        <select name="location" class="form-control border-radius">
                            <option value="">{{__("City")}}</option>
                            @foreach($city as $c)
                                <option
                                    value="{{$c->name}}" @php if(isset($_GET["location"]) && ($_GET["location"] == $c->name))  echo "selected" @endphp>{{__($c->name)}}</option>
                            @endforeach
                        </select>
                    </div>
                @endif

                <div class="col-md-2">
                    <select name="status" class="form-control border-radius">
                        <option value="">{{__("Project status")}}</option>
                        <option
                            value="Not Available" @php if(isset($_GET["status"]) && $_GET["status"] == "Not Available")  echo "selected" @endphp>
                            {{__("Not available")}}
                        </option>
                        <option
                            value="Under Construction" @php if(isset($_GET["status"]) && $_GET["status"] == "Under Construction")  echo "selected" @endphp>
                            {{__("Under Construction")}}
                        </option>
                        <option
                            value="Completed" @php if(isset($_GET["status"]) && $_GET["status"] == "Completed")  echo "selected" @endphp>
                            {{__("Completed")}}
                        </option>
                        <option
                            value="Launching Soon" @php if(isset($_GET["status"]) && $_GET["status"] == "Launching Soon")  echo "selected" @endphp>
                            {{__("Launching Soon")}}
                        </option>
                        <option
                            value="Off-plan" @php if(isset($_GET["status"]) && $_GET["status"] == "Off-plan")  echo "selected" @endphp>
                            {{__("Off-plan")}}
                        </option>
                        <option
                            value="Sold" @php if(isset($_GET["status"]) && $_GET["status"] == "Sold")  echo "selected" @endphp>
                            {{__("Sold")}}
                        </option>
                    </select>
                </div>

                @if($ctg)
                    <div class="col-md-2">
                        <select name="category_id" class="form-control border-radius">
                            <option value="">{{__("Project category")}}</option>
                            @foreach($ctg as $c)
                                <option
                                    value="{{$c->id}}" @php if(isset($_GET["category_id"]) && ($_GET["category_id"] == $c->id))  echo "selected" @endphp>{{__($c->name)}}</option>
                            @endforeach
                        </select>
                    </div>
                @endif
                <div class="col-md-1">
                    <button class="btn btn-primary filter-background" type="submit">
                        <i class="fa fa-sliders"></i>{{__("Filter")}}
                    </button>
                </div>
            </div>
        </div>
        @if($projects)
            <div class="container data-listing data-listing-project">
                {!! Theme::partial('real-estate.projects.items', compact('projects')) !!}
            </div>
        @endif
    </form>
</section>

<section>
    <div class="container-fluid padding-mask-group-custom"
         style="margin-top: 52px;margin-bottom: 94px;padding-right: 0;padding-left: 0;">
        <div class="row">
            <div class="col"><img src="/storage/mask-group-33.png"></div>
            <div class="col" style="text-align: center;"><img src="/storage/mask-group-33.png"></div>
            <div class="col" style="text-align: right;"><img src="/storage/mask-group-33.png"></div>
        </div>
    </div>
</section>

<section>
    <?php echo Theme::partial('our-blog')  ?>
</section>

<!-- Grid row -->
<section>
    {!! Theme::partial('frequently-searched') !!}
</section>



