@php
    if (theme_option('show_map_on_properties_page', 'yes') == 'yes') {
        Theme::asset()->usePath()->add('leaflet-css', 'libraries/leaflet.css');
        Theme::asset()->container('footer')->usePath()->add('leaflet-js', 'libraries/leaflet.js');
        Theme::asset()->container('footer')->usePath()->add('leaflet.markercluster-src-js', 'libraries/leaflet.markercluster-src.js');
    }
@endphp

<!-- check location not found || check location is empty  || check redirect from home page -->
@if(!isset($_GET["location"]) || empty($_GET["location"]) || (isset($_GET["home"])  && ($_GET["home"] == 1)))
    <section class="main-homes pb-3 properties-page">
        <div class="bgheadproject hidden-xs about-us-bg"
             style="background: url('/storage/general/mask-group-39-min.jpg')">
            <div class="description description-custom">
                <div class="container-fluid w90">
                    <h1 class="text-center">{{ SeoHelper::getTitle() }}</h1>
                    <p class="text-center">{{ theme_option('properties_description') }}</p>
                    {!! Theme::partial('breadcrumb') !!}
                </div>
            </div>
        </div>
        <div class="container-fluid w90 padtop30">
            <div class="projecthome">
                <form action="{{ url()->current() }}" method="get" id="ajax-filters-form">
                    <div class="form-style-buy" @if(Auth::user()) style="top: 7%;" @else  style="top: 28%;" @endif>
                        {!! Theme::partial('real-estate.filters.form-buy'); !!}
                    </div>
                    <div class="row rowm10">
                        <div
                            class="@if (theme_option('show_map_on_properties_page', 'yes') == 'yes' && Arr::get($_COOKIE, 'show_map_on_properties', 1)) col-lg-7 left-page-content @else col-lg-12 full-page-content @endif"
                            @if (theme_option('show_map_on_properties_page', 'yes') == 'yes')
                            data-class-full="col-lg-12 full-page-content"
                            data-class-left="col-lg-7 left-page-content"
                            @endif
                            id="properties-list">
                            @include(Theme::getThemeNamespace() . '::views.real-estate.includes.filters', ['isChangeView' => theme_option('show_map_on_properties_page', 'yes') == 'yes'])
                            <div class="data-listing mt-2">
                                {!! Theme::partial('real-estate.properties.items', compact('properties')) !!}
                            </div>
                        </div>
                        @if (theme_option('show_map_on_properties_page', 'yes') == 'yes')
                            <div class="col-md-5 @if (!Arr::get($_COOKIE, 'show_map_on_properties', 1)) d-none @endif"
                                 id="properties-map">
                                <div class="rightmap h-100">
                                    <div
                                        id="map"
                                        data-type="{{ request()->input('type') }}"
                                        data-url="{{ route('public.ajax.properties.map') }}{{ isset($city) && $city ? '?city_id=' . $city->id : '' }}"
                                        data-center="{{ json_encode([43.615134, -76.393186]) }}"></div>
                                </div>
                            </div>
                        @endif
                    </div>
                </form>
            </div>
        </div>
    </section>

    @if (theme_option('show_map_on_properties_page', 'yes') == 'yes')
        <script id="traffic-popup-map-template" type="text/x-custom-template">
        {!! Theme::partial('real-estate.properties.map', ['property' => get_object_property_map()]) !!}
        </script>
    @endif
    <div class="container-fluid padding-mask-group-custom" style="margin-top: 52px;margin-bottom: 94px;padding-right: 0;padding-left: 0;">
        <div class="row">
            <div class="col"><img src="{{url('/storage/mask-group-33.png')}}"></div>
            <div class="col" style="text-align: center;"><img src="{{url('/storage/mask-group-33.png')}}"></div>
            <div class="col" style="text-align: right;"><img src="{{url('/storage/mask-group-33.png')}}"></div>
        </div>
    </div>
    {!! Theme::partial('frequently-searched') !!}
@else
    <section class="main-homes pb-3 properties-page">
        <div class="bgheadproject2 hidden-xs about-us-bg"
             style="background: url(&quot;/storage/general/mask-group-39-min.jpg&quot;);">
            <div class="description description-custom">
                {!! Theme::partial('breadcrumb') !!}
            </div>
            <div class="container">
                <h1 class="text-center panel-area">{{ __($_GET["location"]) }}</h1>
                <p class="text-center">{{ theme_option('properties_description') }}</p>
            </div>
        </div>
        <div class="container-fluid">
            <div class="projecthome">
                <form action="{{ url()->current() }}" method="get" id="ajax-filters-form" class="formmm-area">
                    <div class="form-style-area" @if(Auth::user()) style="top:22%"  @endif>
                        {!! Theme::partial('real-estate.filters.form-area'); !!}
                    </div>
                    <div class="mb-5">
                        <p class="big-title-area big-title-area-responsive">
                            {{__("Our Featured Properties in")}} <span class="project-title"> @php echo __($_GET['location'])  ?? __('Not Found') @endphp </span>
                        </p>
                        <p class="big-title-area-2 big-title-area-2-responsive result-data"> {{count($properties)}} {{__("results")}} </p>
                    </div>
                    <div class="row rowm10">
                        <div
                            class="@if (theme_option('show_map_on_properties_page', 'yes') == 'yes' && Arr::get($_COOKIE, 'show_map_on_properties', 1)) col-lg-7 left-page-content @else col-lg-12 full-page-content @endif"
                            @if (theme_option('show_map_on_properties_page', 'yes') == 'yes')
                            data-class-full="col-lg-12 full-page-content"
                            data-class-left="col-lg-7 left-page-content"
                            @endif id="properties-list">
                            <div class="data-listing mt-2">
                                {!! Theme::partial('real-estate.properties.items', compact('properties')) !!}
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>

    </section>
@endif
