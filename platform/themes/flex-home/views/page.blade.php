@php
    Theme::set('page', $page);
@endphp
@if (in_array($page->template, ['default', 'full-width']))
    <div class="bgheadproject2 hidden-xs about-us-bg background-repeat-custom"
         style="background: url('{{ theme_option('breadcrumb_background') ? RvMedia::url(theme_option('breadcrumb_background')) : Theme::asset()->url('images/banner-du-an.jpg') }}')">
        <div class="description description-custom">
            <div class="container-fluid w90">
                {!! Theme::partial('breadcrumb') !!}
            </div>
        </div>

        @if(strpos("sell with us , contact us", strtolower(SeoHelper::getTitle())))
            @php
                $string = SeoHelper::getTitle();
                $last_word_start = strrpos($string, ' ') + 1; // +1 so we don't include the space in our result
                $last_word = substr($string, $last_word_start);
                $string=trim($string, $last_word);
            @endphp
            <div class="container container_project">
                <h1 class="text-left panel-sell-with-us">{{ $string }}<b> {{ $last_word }} </b></h1>
                <p class="text-left">{{ theme_option('properties_description') }}</p>
            </div>
        @else
            <div class="container container_project">
                <h1 class="text-left panel-sell-with-us">{{SeoHelper::getTitle()}}</b></h1>
                <p class="text-left">{{ theme_option('properties_description') }}</p>
            </div>
        @endif
    </div>
    <div class="@if ($page->template != 'full-width') container @endif padtop50">
        <div class="row">
            <div class="col-sm-12">
                <div class="scontent">
                    {!! apply_filters(PAGE_FILTER_FRONT_PAGE_CONTENT, BaseHelper::clean($page->content), $page) !!}
                </div>
            </div>
        </div>
    </div>
    <br>
    <br>
@elseif (in_array($page->template, ['about-us']))
    <div class="bgheadproject2 hidden-xs about-us-bg" style="background: url('/storage/general/mask-group-39-min.jpg')">
        <div class="description description-custom">
            <div class="container-fluid w90">
                 {!! Theme::partial('breadcrumb') !!}
            </div>
        </div>
        <div class="container container_project">
            <h1 class="text-left panel-sell-with-us">{{ SeoHelper::getTitle() }}</h1>
            <p class="text-left">{{ theme_option('properties_description') }}</p>
        </div>
    </div>
    <div class="container-fluid text-center d-xxl-flex justify-content-xxl-center about-us-sub-menu d-flex align-items-center justify-content-center">
        <ul class="nav align-items-end">
        <li class="nav-item about-us-menu" style="color: #034D8A;"><a class="nav-link" href="#about-us" style="color: #034D8A;">{{__('About US')}}</a></li>
        <li class="nav-item about-us-menu"><a class="nav-link" href="#our-service" style="color: #034D8A;">{{__('Our Services')}} </a></li>
        <li class="nav-item about-us-menu"><a class="nav-link" href="#marketing-and-ads" style="color: #034D8A;">{{__('Marketing and Ads')}} </a></li>
        <li class="nav-item about-us-menu"><a class="nav-link" href="#our-team" style="color: #034D8A;">{{__('Our Team')}} </a></li>
        </ul>
    </div>
    <div class="@if ($page->template != 'full-width') container-fluid @endif padtop50">
        <div class="row">
            <div class="col-sm-12">
                <div class="scontent">
                    {!! apply_filters(PAGE_FILTER_FRONT_PAGE_CONTENT, BaseHelper::clean($page->content), $page) !!}
                </div>
            </div>
        </div>
    </div>
    <br>
    <br>
@elseif (in_array($page->template, ['blog']))
<div class="bgheadproject2 hidden-xs blog-bg" style="background: url('/storage/about-us.jpg')">
    <div class="description description-custom">
        <div class="container-fluid w90">
                 {!! Theme::partial('breadcrumb') !!}
        </div>
    </div>
    <div class="container container_project">
        <h1 class="text-left panel-sell-with-us">{{ SeoHelper::getTitle() }}</h1>
        <p class="text-left">{{ theme_option('properties_description') }}</p>
    </div>
</div>

<div class="@if ($page->template != 'full-width') container @endif padtop50">
    <div class="row">
        <div class="col-sm-12">
            <div class="scontent">
                {!! apply_filters(PAGE_FILTER_FRONT_PAGE_CONTENT, BaseHelper::clean($page->content), $page) !!}
            </div>
        </div>
    </div>
</div>
<br>
<br>
@else
    {!! apply_filters(PAGE_FILTER_FRONT_PAGE_CONTENT, BaseHelper::clean($page->content), $page) !!}
@endif
