<?php echo Theme::partial('header') ?>
<?php if (app()->getLocale() == "en") { ?>
    <div id="app">
        <div id="is-about-us">
            <div class="bgheadproject2 hidden-xs about-us-bg"
                 style="background: url('/storage/about-us.jpg')">
                <div class="description description-custom">
                    <div class="container-fluid w90">
                        <?php echo Theme::partial('breadcrumb') ?>
                    </div>
                </div>
                <?php
                if(app()->getLocale()=="en"){
                    $string = SeoHelper::getTitle();
                    $last_word_start = strrpos($string, ' ') + 1; // +1 so we don't include the space in our result
                    $last_word = substr($string, $last_word_start);
                    $string = trim($string, $last_word);
                }else{
                    $string = SeoHelper::getTitle();
                    $last_word='';
                }
                ?>
                <div class="container container_project">
                    <h1 class="text-left panel-sell-with-us"><?= $string ?><b> <?= $last_word ?> </b></h1>
                    <p class="text-left"><?php echo theme_option('properties_description') ?></p>
                </div>
            </div>
            <div
                class="container-fluid text-center d-xxl-flex justify-content-xxl-center about-us-sub-menu d-flex align-items-center justify-content-center">
                <ul class="nav align-items-end">
                    <li class="nav-item about-us-menu" style="color: #034D8A;"><a class="nav-link" href="#about-us"
                                                                                  style="color: #034D8A;"><?php echo __('About us') ?></a>
                    </li>
                    <li class="nav-item about-us-menu"><a class="nav-link" href="#our-service"
                                                          style="color: #034D8A;"><?php echo __('Our Services') ?> </a></li>
                    <li class="nav-item about-us-menu"><a class="nav-link" href="#marketing-and-ads"
                                                          style="color: #034D8A;"><?php echo __('Marketing and Ads') ?> </a>
                    </li>
                    <li class="nav-item about-us-menu"><a class="nav-link" href="#our-team"
                                                          style="color: #034D8A;"><?php echo __('Our Team') ?> </a></li>
                </ul>
            </div>
            <div class="container-fluid  padtop50">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="scontent">
                            <div class="about-us-wrap about-us-wrap">
                                <div class="container container-margin" id="about-us"><h1 style="color:rgb(3,77,138);">Who
                                        We are?</h1>
                                    <p><img class="d-xxl-flex float-start" src="<?= url('/storage/general/N.png') ?>" alt="N.png">Nothing
                                        has neither been
                                        constructed nor built without passion. Passion moves mountains, makes the impossible
                                        possible, and helps
                                        dreams become a concretized reality. That is exactly how SAMA Emirates Properties, a real estate</p>
                                    <p>company like no other, has started over 20 years ago. Located in Abu Dhabi, SAMA Emirates Properties
                                        is the huge real estate
                                        company that has managed to cross borders and meet the clients’ expectations. SAMA
                                        UAE is more than a simple
                                        real estate company; it is a group of top real estate companies who have and own
                                        properties across the
                                        United Arab Emirates.&nbsp;</p>
                                    <p>SAMA Emirates Properties is the leading and the largest real estate company in Abu Dhabi, the United
                                        Arab Emirates. Used
                                        interchangeably with the word “luxury”, the owners of this top-notch real estate
                                        company do indeed represent
                                        the heritage of the United Arab Emirates.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br><br>&nbsp;
                                    </p>
                                    <p>&nbsp;SAMA Emirates Properties is a real estate company made out of passionate traders, creative
                                        agents, and problem-solving
                                        individuals who always go above and beyond to offer their clients what they need.
                                        All clients are served
                                        with joy, treated with respect, and valued. The real estate company tries to tailor
                                        the needs of the clients
                                        based on the budget, the preference, and the details that they care about the most.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br><br>&nbsp;
                                    </p></div>
                                <div class="container"><h1 style="color:rgb(3,77,138);">Get to Know Us Better</h1>
                                    <div class="row">
                                        <div class="col-sm-12 col-md-7"><p>&nbsp;</p>
                                            <p>One can never truly know about the company unless there is enough information
                                                about the vision, the
                                                mission, and the target audience. They are the key factors that can never be
                                                overlooked.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
                                        </div>
                                        <div class="col-sm-12 col-md-5">
                                            <blockquote><p style="color:rgb(141,141,141);">Thank you</p>
                                                <p style="color:rgb(141,141,141);"><span style="color:rgb(0,0,0);"><span>&nbsp;“In a world full of noise, SAMA and our affiliates have been able to stand out and offer the best service”&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</span></span>
                                                </p></blockquote>
                                            <p>&nbsp;</p>
                                            <p style="line-height:30px;">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                                &nbsp; &nbsp;</p></div>
                                    </div>
                                </div>
                                <div class="container"><h1 style="color:rgb(3,77,138);font-size:26px;font-weight:100;">Our
                                        Vision</h1>
                                    <p style="color:rgb(53,113,161);font-size:32px;font-weight:bolder;">Marking your
                                        territories!</p>
                                    <div class="row">
                                        <div class="col"><p>&nbsp;</p>
                                            <p>Since we have been marking territories for over 20 years, our vision has
                                                always been unique. We aim
                                                at expanding across the United Arab Emirates, the country that includes the
                                                major cosmopolitan hubs
                                                in the Middle East and becoming more than a simple real estate agency. We
                                                too have dreams, that is
                                                why, our dream is to become a real estate company that everyone trusts,
                                                relies on, and refers to
                                                when it comes to finding a professional property agent.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>&nbsp;
                                            </p></div>
                                    </div>
                                </div>
                                <div class="container"><h1 style="color:rgb(3,77,138);font-size:26px;font-weight:100;">Our
                                        Mission</h1>
                                    <p style="color:rgb(53,113,161);font-size:32px;font-weight:bolder;">Ease the burden of
                                        finding a property..</p>
                                    <div class="row">
                                        <div class="col"><p>&nbsp;</p>
                                            <p>Our mission is to ease the burden of finding a property to buy or to rent and
                                                to always come up with
                                                the best solutions for everyone. We definitely know that many of you are
                                                already burning the candle
                                                at both ends, living hectic lives, and trying to find balance between social
                                                and professional life.
                                                Therefore, at SAMA Emirates Properties, our first and foremost goal is to assist clients in
                                                finding the dream house
                                                they are looking for and in renting out or selling their property. We lift
                                                our own employees so they
                                                can lift you too.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>&nbsp;
                                            </p></div>
                                    </div>
                                </div>
                                <div class="container"><h1 style="color:rgb(3,77,138);font-size:26px;font-weight:100;">Our
                                        Target Audience</h1>
                                    <p style="color:rgb(53,113,161);font-size:32px;font-weight:bolder;">Our services are
                                        offered to everyone.</p>
                                    <div class="row">
                                        <div class="col"><p>&nbsp;</p>
                                            <p>Whether rich individuals or traders who want to buy a property in the United
                                                Arab Emirates, our real
                                                estate company can be accessed by all of you. Our real estate company is
                                                home for investors who wish
                                                to benefit from the cosmopolitan hubs in the United Arab Emirates, families
                                                who wish to own a house,
                                                or even rich individuals who need to buy many properties to be able to use
                                                them in the future.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>&nbsp;
                                            </p></div>
                                    </div>
                                </div>
                                <div class="container text-center">
                                    <img src="<?= url('/storage/general/path-5409.png') ?>" alt="Path%205409.png"></div>
                                <div class="container text-center">
                                    <h2 style="color:rgb(3,77,138);" class="about-us-all-title">SAMA Services</h2>
                                    <p style="text-align:left;">We wholeheartedly believe that our services will satisfy our
                                        potential clients, just
                                        like previous clients were satisfied. To take up residence in your dream house, buy
                                        your property, rent out
                                        your apartment, or even rent a flat is literally a milestone that you must be proud
                                        of. Remember, success
                                        comes in waves. Here is a glimpse of what our services as a professional real estate
                                        company include:</p>
                                </div>
                                <div id="our-service" class="container-fluid text-center about-us-section"
                                     style="background-color:rgb(219,225,232);width:1400px;">
                                    <p><img class="line-1" src=<?= url("/storage/general/Group-4014.svg") ?> alt="">
                                        <img class="line-2" src=<?= url("/storage/general/Group-4013.svg") ?>
                                                                                                             alt="">
                                        <img class="line-3" src="<?= url('/storage/general/group-4015-1.svg') ?>" alt="Group%204015%20(1).svg">
                                        <img class="line-4" src="<?= url('/storage/general/group-4015-1.svg')?>"
                                            alt="Group%204015%20(1).svg"></p>
                                    <div class="row">
                                        <div class="col"><img src="<?= url('/storage/general/about1.png') ?>" alt="about1.png"></div>
                                        <div class="col"><h1 class="title-slide-about-us" style="color:rgb(3,77,138);">
                                                <strong>Management</strong></h1>
                                            <p style="color:rgb(3,77,138);font-size:18px;" class="title-slide-about-us">-
                                                Facility management&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>-
                                                Rental management&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>-
                                                Property management</p></div>
                                    </div>
                                    <div class="row">
                                        <div class="col"><h1 class="title-slide-about-us" style="color:rgb(3,77,138);">
                                                <strong>Buy and Sell</strong></h1>
                                            <p style="color:rgb(3,77,138);font-size:18px;" class="title-slide-about-us">-
                                                Negotiations&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>-
                                                Property showcase&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>-
                                                Listings&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>-
                                                Process of selling, buying, renting, and renting out a property</p></div>
                                        <div class="col oposite"><img src="<?= url('/storage/general/about2.png')?>" alt="about2.png">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col oposite-2"><img src="<?= url('/storage/general/about3.png') ?>" alt="about3.png">
                                        </div>
                                        <div class="col"><h1 class="title-slide-about-us" style="color:rgb(3,77,138);">
                                                <strong>Selling and
                                                    Refinancing</strong><br>&nbsp;</h1>
                                            <p class="title-slide-about-us" style="color:rgb(3,77,138);font-size:18px;">-
                                                Property maintenance&nbsp;<br>-Marketing
                                                and ads</p></div>
                                    </div>
                                    <div class="row">
                                        <div class="col"><h1 class="title-slide-about-us" style="color:rgb(3,77,138);">
                                                <strong>Consultancy</strong><br>&nbsp;</h1>
                                            <p class="title-slide-about-us" style="color:rgb(3,77,138);font-size:18px;">
                                                -Real estate
                                                consultancy&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>-
                                                Investment advisory&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>-
                                                Property valuation</p></div>
                                        <div class="col"><img src="<?= url('/storage/general/about4.png') ?>" alt="about4.png"></div>
                                    </div>
                                </div>
                                <div class="container"><h1 class="oposite-2" style="font-size:40px;"
                                                           class="about-us-all-title">UNIQUE: Six Letters, One Company</h1>
                                    <div class="row">
                                        <div class="col-md-auto"><img src="<?= url('/storage/general/we.png') ?>" alt="We.png"></div>
                                        <div class="col-md-6"><p>have always believed that the word “unique” is more than
                                                just a six-letter word.
                                                That is why we have built an actual representation for everyone to see, so
                                                the next time someone
                                                mentions the word “unique”, you know what&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>example
                                                to give.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>&nbsp;
                                            </p>
                                            <p style="font-size:19px;font-weight:bolder;">We are Unique, because we care
                                                about YOU!&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>&nbsp;
                                            </p></div>
                                    </div>
                                </div>
                                <div class="container"><h1 style="color:rgb(135,181,214);font-weight:bolder;">Knowledge
                                        &amp; Expertise</h1>
                                    <p>While foreseeing the future is impossible, foreseeing future problems is possible
                                        with SAMA Emirates Properties. Our over 20
                                        years of experience in the real estate industry allows us to help clients prevent
                                        future problems. We are
                                        able to use our past experiences in your future projects.</p></div>
                                <div class="container"><h1 style="color:rgb(135,181,214);font-weight:bolder;">24/7</h1>
                                    <p>SAMA Emirates Properties is a real estate company that is available 24/7. This is indeed one of the
                                        features that clients
                                        like the most about us. We are always ready to help you and answer your questions
                                        and talk you out of
                                        concerns and anxiety.</p></div>
                                <div class="container"><h1 style="color:rgb(135,181,214);font-weight:bolder;">
                                        Reliability</h1>
                                    <p>Our real estate company is a reliable company with reliable real estate agents. Each
                                        real estate agent is
                                        transparent, honest, and straightforward. That being said, clients do trust us and
                                        we never fail at making
                                        them achieve their goals.</p></div>
                                <div class="container"><h1 style="color:rgb(135,181,214);font-weight:bolder;">
                                        Commitment</h1>
                                    <p>Our committed real estate agency is all clients are looking for. Thanks to our
                                        passionate real estate agents,
                                        our services are professional and we are committed to offering services that are the
                                        substitute for the word
                                        perfection.</p></div>
                                <div class="container-fluid marketing-ads-section" id="marketing-and-ads">
                                    <h1 style="color:rgb(255,255,255);text-align:center;">Marketing
                                        and Ads</h1>
                                    <section class="container marketing-text-box">
                                        <div class="row" style="background-color:rgb(255,255,255);">
                                            <?php
                                            if (app()->getLocale() == "ar")
                                                $url = "/ar/buy-properties";
                                            else
                                                $url = "/buy-properties";
                                            ?>
                                            <div class="col"><p style="font-size:20px;" class="slide-about-us-markting">
                                                    Obviously, in a world full of new trends and with technology
                                                    taking over our minds, SAMA Emirates Properties offers its clients marketing services
                                                    and ads to be able to
                                                    attract potential buyers, renters, or investors.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br><br>That
                                                    being said, we handle the task of marketing your property in the best
                                                    way to get the best offer.
                                                    That is done by our professionals and social media experts who know the
                                                    key factors that must be
                                                    highlighted. Our experts do their utmost to offer clients the best
                                                    marketing strategies.</p>
                                                <p class="mt-4"><a
                                                        style="border-radius: 5px; border: 2px solid rgb(3, 77, 138); color: rgb(3, 77, 138); font-size: 24px; padding: 0.5% 3%; text-decoration: none;"
                                                        href="<?= url($url) ?>"
                                                        class="buy-font"><?php echo __("Buy your dream property!") ?></a>
                                                </p></div>
                                        </div>
                                    </section>
                                </div>
                                <div class="container text-center sama-logo-banner">
                                    <img src="<?= url('/storage/general/path-5409.png') ?>" alt="Path%205409.png">
                                </div>
                                <div class="container our-values">
                                    <section style="min-height:669px;"><h1
                                            style="color:rgb(159,162,165);font-size:50px;font-weight:500;text-align:right;width:200px;">
                                            What&nbsp;&nbsp;<br>Are&nbsp;&nbsp;<br>Our
                                            Values?</h1>
                                        <div class="content_img">
                                            <img class="our-values-image-1" src="<?= url('/storage/about/Honsety.png') ?>" alt="Component%2048%20–%201.png">
                                            <div class="text-what-value"> Honesty </div>
                                            <span class="span1over span1over1" style="font-size:20px;height:400px;padding-left:60px;text-align:center;width:98%;">honesty and transparency are both the base of our real estate company</span>
                                        </div>
                                        <div class="content_img2" style="width:380px;">
                                            <img class="our-values-image-2" src="<?= url('/storage/about/Accountability.png')?> " alt="Component%2049%20–%201.png">
                                            <div class="text-what-value-1"> Accountability </div>
                                            <span class="span1over span4over4" style="font-size:20px;height:500px;padding-left:60px;text-align:center;width:98%;">We take full responsibility for our actions and decisions</span>
                                        </div>
                                        <div class="content_img3" style="width:550px;">
                                            <img class="our-values-image-3" src="<?= url('/storage/about/Integrity.png') ?>" alt="Component%2050%20–%201.png">
                                            <div class="text-what-value"> Integrity </div>
                                            <span class="span1over span3over3"
                                                style="font-size:20px;height:250px;padding-left:60px;text-align:center;width:89%;">A real estate company can never exist without integrity as a core value</span>
                                        </div>
                                        <div class="content_img4" style="width:400px;">
                                            <img class="our-values-image-4" src="<?= url('/storage/about/Value-centricity.png') ?>" alt="Component%2051%20–%201.png">
                                            <div class="text-what-value-1"> Value-centricity </div>
                                            <span class="span1over span2over2" style="font-size:20px;height:333px;padding-left:60px;text-align:center;width:92%;">We only move based on our values and principles</span>
                                        </div>
                                        <p class="our-values-at-sama" style="font-size:26px;width:248px;">Our values at SAMA
                                            UAE are what clients
                                            like the most.</p>
                                    </section>
                                </div>
                                <div class="container" id="our-team">
                                    <h2 style="color:rgb(3,77,138);font-size:40px;" class="about-us-all-title">Our Team</h2>
                                    <p>Our team includes skillful individuals who also identify as passionate agents.
                                        Typically, the team of SAMA
                                        UAE revolves around talented engineers, expert surveyors, knowledgeable advisors and
                                        consultants, and more.
                                        <strong>Here are our team members</strong>:</p></div>
                                <div class="container">
                                    <div class="row" style="background-color:rgb(240,247,255);">
                                        <div class="col-md-4">
                                            <img src="<?= url('/storage/general/Aref Ismail Al Khoori.jpg') ?> "
                                                   style="max-height:268.625px !important;max-width: 255px !important;border-radius: 30px !important;" alt="Small%20teaser.png"></div>
                                        <div class="col-md-8"><h1 class="about-us-title-person-team">Aref Ismail Al
                                                Khoori</h1>
                                            <p><span>CEO</span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>&nbsp;
                                            </p>
                                            <p>The CEO of SAMA Emirates Properties is Aref Ismail Al Khoori. For over 14 years, Aref Ismail
                                                Al Khoori has been an
                                                influencing figure in the United Arab Emirates’ business community. That
                                                being said, he has already
                                                served as a member of the board of directors in Islamic insurance companies,
                                                Islamic banks and
                                                financial institutions, and even real estate entities.</p>
                                            <div>
                                                <p>
                                                    <button class="btn btn-link btn-sm read-more">read more</button>
                                                </p>
                                                <div class="collapse"><p>This prominent figure in the United Arab Emirates’
                                                        financial and
                                                        engineering industries, banking, and property is currently serving
                                                        as the Chairman for SAMA
                                                        UAE Properties, SAMA Financial Consultancy, and Canal Engineering
                                                        Services. Aref Ismail Al
                                                        Khoori is a holder of an M.B.A with specialization in Finance and
                                                        Accounting.</p></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="container team-section">
                                    <div class="row">
                                        <div class="col-md-6" style="text-align:center;"><p>
                                                <img class="our-time-img" src="<?= url('/storage/general/hazem_mohamed.jpg')?>" alt="Small%20teaser2.png">
                                            </p>
                                            <div class="text-our-team"><h1 style="font-size:30px;"
                                                                           class="about-us-title-person-team">Hazem
                                                    Mohamed</h1>
                                                <p><span>Sales Agent</span>&nbsp;<br>&nbsp;</p></div>
                                        </div>
                                        <div class="col-md-6" style="text-align:center;">
                                            <p>
                                                <img class="our-time-img" src="<?= url('/storage/general/karim_fayez.jpg')?>" alt="Small%20teaser3.png">
                                            </p>
                                            <div class="text-our-team"><h1 style="font-size:30px;"
                                                                           class="about-us-title-person-team">Karim
                                                    Fayez</h1>
                                                <p><span>Sales Agent</span>&nbsp;<br>&nbsp;</p></div>
                                        </div>
                                        <div class="col-md-6" style="text-align:center;">
                                            <p>
                                                <img class="our-time-img" src="<?= url('/storage/general/malika.jpg')?>" alt="Small%20teaser4.png">
                                            </p>
                                            <div class="text-our-team"><h1 style="font-size:30px;"
                                                                           class="about-us-title-person-team">Malika Zaidi</h1>
                                                <p><span>Sales Agent</span>&nbsp;<br>&nbsp;</p></div>
                                        </div>
                                        <div class="col-md-6" style="text-align:center;">
                                            <p>
                                                <img class="our-time-img" src="<?= url('/storage/general/soukaina.jpg')?>" alt="Small%20teaser4.png">
                                            </p>
                                            <div class="text-our-team"><h1 style="font-size:30px;"
                                                                           class="about-us-title-person-team">	Soukaina Kbida </h1>
                                                <p><span>Sales Agent</span>&nbsp;<br>&nbsp;</p></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="container sell-with-us sell-with-us-custom about-us-border"
                                     style="background-color:rgb(227,238,246);height:477px;">
                                    <div class="Buy-Rent"><h2
                                            style="color:rgb(20,138,200);font-size:40px;font-weight:bolder;"
                                            class="about-us-all-title">Looking to
                                            buy/rent a property in UAE?</h2>
                                        <p style="font-size:18px;" class="about-us-content-1">Now you are offered the chance
                                            to choose between many property
                                            options to find the ideal property suitable for your family lifestyle! It is
                                            your decision to make but
                                            it’s our duty to help you!</p>
                                        <p style="font-size:30px;" class="just about-us-content-1">Just remember, lights
                                            will NOT guide you home, but SAMA Emirates Properties
                                            definitely will!</p></div>

                                    <?php


                                    if (app()->getLocale() == "ar"){
                                        $urlSale = "/ar/buy-properties";
                                        $urlRent = "/ar/rent-properties";
                                    } else {
                                        $urlSale = "/buy-properties";
                                        $urlRent = "/rent-properties";
                                    }


                                    ?>

                                    <div class="mt-4">
                                        <div class="col mt-4" style="text-align:center;"><a
                                                style="border-radius: 5px; border: 2px solid rgb(4, 78, 138); color: rgb(4, 78, 138); padding: 0.5% 7%; text-align: center;"
                                                href="<?=url($urlSale)?>"><?php echo __("Buy now") ?></a></div>
                                        <div class="col mt-4" style="text-align:center;"><a
                                                style="border-radius: 5px; border: 2px solid rgb(4, 78, 138); color: rgb(4, 78, 138); padding: 0.5% 7%; text-align: center;"
                                                href="<?=url($urlRent)?>"><?php echo __("Rent now") ?></a></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <br>
            <br>
        </div>
    </div>
<?php }
else { ?>
    <div id="app">
        <div id="is-about-us">
            <div class="bgheadproject2 hidden-xs about-us-bg"
                 style="background: url('/storage/about-us.jpg')">
                <div class="description description-custom">
                    <div class="container-fluid w90">
                        <?php echo Theme::partial('breadcrumb') ?>
                    </div>
                </div>
                <?php
                if (app()->getLocale() == "en") {
                    $string = SeoHelper::getTitle();
                    $last_word_start = strrpos($string, ' ') + 1; // +1 so we don't include the space in our result
                    $last_word = substr($string, $last_word_start);
                    $string = trim($string, $last_word);
                } else {
                    $string = SeoHelper::getTitle();
                    $last_word = '';
                }
                ?>
                <div class="container container_project">
                    <h1 class="text-left panel-sell-with-us"><?= $string ?><b> <?= $last_word ?> </b></h1>
                    <p class="text-left"><?php echo theme_option('properties_description') ?></p>
                </div>
            </div>
            <div
                class="container-fluid text-center d-xxl-flex justify-content-xxl-center about-us-sub-menu d-flex align-items-center justify-content-center">
                <ul class="nav align-items-end">
                    <li class="nav-item about-us-menu" style="color: #034D8A;"><a class="nav-link" href="#about-us"
                                                                                  style="color: #034D8A;"><?php echo __('About us') ?></a>
                    </li>
                    <li class="nav-item about-us-menu"><a class="nav-link" href="#our-service"
                                                          style="color: #034D8A;"><?php echo __('Our Services') ?> </a>
                    </li>
                    <li class="nav-item about-us-menu"><a class="nav-link" href="#marketing-and-ads"
                                                          style="color: #034D8A;"><?php echo __('Marketing and Ads') ?> </a>
                    </li>
                    <li class="nav-item about-us-menu"><a class="nav-link" href="#our-team"
                                                          style="color: #034D8A;"><?php echo __('Our Team') ?> </a></li>
                </ul>
            </div>
            <div class="container-fluid  padtop50">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="scontent">
                            <div class="about-us-wrap about-us-wrap">
                                <div class="container container-margin" id="about-us">
                                    <h1 style="color:rgb(3,77,138);">من نكون؟</h1>
                                    <p>بالشغف يصبح المستحيل ممكنًا وتغدو الأحلام واقعًا، فهو المعين على إتمام أي مسعى، وهو
                                        الشرارة التي انطلقت منها شركة عقارات سما الإمارات قبل 20 سنة، واستمرت في النمو
                                        حتى وصل نشاطها إلى كافة أنحاء دولة الإمارات العربية المتحدة.</p>
                                    <p>تقع شركة سما الإمارات في إمارة أبوظبي، وهي شركة غيرت بخدماتها شكل السوق العقاري،
                                        فلبت احتياجات عملائها وتخطت كل ما هو متوقع، وليست شركة سما كأي شركة أخرى، فهي
                                        اتحاد لمجموعة من كبرى الشركات التي تمتلك عقارات في مختلف أنحاء الإمارات العربية
                                        المتحدة.</p>
                                    <p>بلغت شركتنا من التميز مستوى جعل من اسمها عنوانًا للتميز والثقة، ورغم كونها أكبر
                                        الشركات العقارية في أبوظبي إلا أن خدماتها بقيت مكتسية بالتراث الإماراتي العريق.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br><br>&nbsp;
                                    </p>
                                    <p>&nbsp;أما أعضاء شركة عقارات سما فهم محترفون يملؤهم الشغف، وخبراء متقنون لحرفتهم،
                                        وأفراد يمتلكون حلولًا مبتكرة لأصعب المشكلات، وهم على أتم الاستعداد لبذل أكبر
                                        الجهود في سبيل تلبية احتياجات العملاء.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br><br>&nbsp;
                                    </p>
                                    <p>يقوم تعاملنا مع عملائنا على أساس الحب والتقدير والاحترام، ونحاول في شركتنا توفير
                                        طيف واسع من الاختيارات التي تناسب القدرات المادية للجميع، آخذين بعين الاعتبار
                                        ذوق العميل الخاص والتفاصيل التي يبني عليها اختياره.</p>
                                </div>
                                <div class="container"><h1 style="color:rgb(3,77,138);">اعرف المزيد عنا</h1>
                                    <div class="row">
                                        <div class="col-sm-12 col-md-7"><p>&nbsp;</p>
                                            <p>لا بد لأي فرد يرغب بالتعامل مع شركة عقارية أن يعرف كمًا وافيا من
                                                المعلومات عن رؤيتها ومهمتها ونوع عملائها، إذ أن هذه العناصر تعينه على
                                                اختيار الشركة المناسبة لاحتياجاته الخاصة. تابع معنا لتعرف
                                                المزيد.&nbsp;</p>
                                        </div>
                                        <div class="col-sm-12 col-md-5">
                                            <blockquote><p style="color:rgb(141,141,141);">شكرًا لك</p>
                                                <p style="color:rgb(141,141,141);"><span
                                                        style="color:rgb(0,0,0);"><span>في عالم مليء بالصخب، تمكنت سما الإمارات من تحقيق التميّز بتقديم أفضل الخدمات العقارية في الإمارات!</span></span>

                                                </p></blockquote>
                                            <p>&nbsp;</p>
                                            <p style="line-height:30px;">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                                &nbsp;
                                                &nbsp; &nbsp;</p></div>
                                    </div>
                                </div>
                                <div class="container">
                                    <h1 style="color:rgb(3,77,138);font-size:26px;font-weight:100;">رؤيتنا</h1>
                                    <h1 style="color:rgb(3,77,138);font-size:20px;font-weight:100;">نضع حدود مملكتك</h1>
                                    <div class="row">
                                        <div class="col"><p>&nbsp;</p>
                                            <p>تميزت رؤيتنا عن غيرنا في هذا المجال منذ أن بدأنا بشق طريقنا قبل أكثر من
                                                20 سنة، واضعين نصب أعيننا هدفًا واحدًا ألا وهو الاستمرار بالتوسع إلى أن
                                                تغطي فروعنا كافة أنحاء دولة الإمارات العربية المتحدة؛ تلك الدولة التي
                                                باتت مركزًا اقتصاديًا رائدًا تتباهى كبرى الشركات العالمية بامتلاك فروعٍ
                                                فيه، أما غايتنا الأسمى فهي أن نصبح الشركة التي يعتمد عليها الجميع ويضعون
                                                فيها ثقتهم، وتكون خيارهم الأول كلما رغبوا بالبحث عن خبراء عقاريين.
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>&nbsp;
                                            </p></div>
                                    </div>
                                </div>
                                <div class="container">
                                    <h1 style="color:rgb(3,77,138);font-size:26px;font-weight:100;">مهمّتنا</h1>
                                    <h1 style="color:rgb(3,77,138);font-size:20px;font-weight:100;">لا تشغل بالك بموضوع البحث عن عقار</h1>
                                    <div class="row">
                                        <div class="col"><p>&nbsp;</p>
                                            <p>نعلم تمامًا أن الغالبية العظمى من الناس تحيا حياة شاقة ملؤها التعب
                                                والإرهاق، ويحاولون بأقصى جهدهم تحقيق التوازن ما بين حياتهم المهنية
                                                والاجتماعية، ولذا، فإن مهمتنا في سما الإمارات تتمثل بتسهيل عملية شراء
                                                العقارات وتأجيرها، إضافة إلى توفير طيفٍ واسعٍ من الخيارات يناسب الجميع
                                                مهما اختلفت أذواقهم وقدراتهم المادية، وبذلك نساعد عملاءنا في رحلتهم نحو
                                                شراء منزل أحلامهم أو استئجار العقارات أو بيعها.<br>&nbsp;</p></div>
                                    </div>
                                </div>
                                <div class="container">
                                    <h1 style="color:rgb(3,77,138);font-size:26px;font-weight:100;">عملاؤنا</h1>
                                    <h1 style="color:rgb(3,77,138);font-size:20px;font-weight:100;">خدمة من القلب للكل!</h1>
                                    <div class="row">
                                        <div class="col"><p>&nbsp;</p>
                                            <p>تقدّم شركة عقارات سما الإمارات خدماتها للجميع من دون استثناء، سواءً كانوا
                                                أشخاصًا ميسورين ماديًّا أو تجّارًا يريدون شراء عقارات للاستفادة في وجود
                                                فروع للشركات العالمية في دولة الإمارات العربيّة المتّحدة، إضافة إلى
                                                العائلات التي تريد امتلاك منزل، وحتى الأثرياء الذين يريدون شراء عدد من
                                                العقارات لاستخدامها مستقبلًا.<br>&nbsp;
                                            </p></div>
                                    </div>
                                </div>
                                <div class="container text-center">
                                    <img src="<?= url('storage/general/path-5409.png') ?>" alt="Path%205409.png">
                                </div>
                                <div class="container text-center">
                                    <h2 style="color:rgb(3,77,138);" class="about-us-all-title">خدماتنا</h2>
                                    <p style="text-align:right;">نثق بأن ما نقدمه من خدمات متميزة كفيل بحصد رضى عملائنا،
                                        كما كان الحال دائمًا، وسواء كنت مقْدمًا على مشروع كبير كشراء المنزل الذي تحلم به
                                        أو شراء عقار أو أمور أبسط من ذلك كاستئجار شقة أو تأجيرها، عليك أن تكون فخورًا
                                        بنفسك، فما النجاح إلا حصيلة تراكم الإنجازات الصغيرة. إليكم بعض الخدمات المتميزة
                                        التي نقدمها لعملائنا:</p>
                                </div>
                                <div id="our-service" class="container-fluid text-center about-us-section"
                                     style="background-color:rgb(219,225,232);width:1400px;">
                                    <p><img class="line-1" src="<?= url('/storage/general/Group-4014.svg')?>" alt=""><img
                                            class="line-2"
                                            src="<?= url('/storage/general/Group-4013.svg') ?>"
                                            alt=""><img
                                            class="line-3"
                                            src="<?= url('/storage/general/group-4015-1.svg') ?>"
                                            alt="Group%204015%20(1).svg"><img
                                            class="line-4" src="<?= url('/storage/general/group-4015-1.svg') ?>"
                                            alt="Group%204015%20(1).svg"></p>
                                    <div class="row">
                                        <div class="col"><img src="<?= url('/storage/general/about1.png') ?>" alt="about1.png"></div>
                                        <div class="col"><h1 class="title-slide-about-us" style="color:rgb(3,77,138);">
                                                <strong>الإدارة</strong></h1>
                                            <p style="color:rgb(3,77,138);font-size:18px;" class="title-slide-about-us">
                                                -
                                                إدارة المرافق&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>-
                                                إدارة الممتلكات&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>-
                                                إدارة العقارات المستأجرة</p></div>
                                    </div>
                                    <div class="row">
                                        <div class="col"><h1 class="title-slide-about-us" style="color:rgb(3,77,138);">
                                                <strong>الشراء والبيع: </strong></h1>
                                            <p style="color:rgb(3,77,138);font-size:18px;" class="title-slide-about-us">
                                                -
                                                التفاوض&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>-
                                                عرض الممتلكات&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>-
                                                إدراج العقارات&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>-
                                                بيع العقارات وشرائها واستئجارها وتأجيرها </p></div>
                                        <div class="col oposite">
                                            <img src="<?= url('/storage/general/about2.png') ?>" alt="about2.png">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col oposite-2">
                                            <img src="<?= url('/storage/general/about3.png') ?>" alt="about3.png">
                                        </div>
                                        <div class="col"><h1 class="title-slide-about-us" style="color:rgb(3,77,138);">
                                                <strong> البيع وإعادة التمويل</strong><br>&nbsp;</h1>
                                            <p class="title-slide-about-us" style="color:rgb(3,77,138);font-size:18px;">
                                                -
                                                صيانة الممتلكات <br>
                                                التسويق والإعلانات
                                            </p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col"><h1 class="title-slide-about-us" style="color:rgb(3,77,138);">
                                                <strong>الخدمات الاستشارية</strong><br>&nbsp;</h1>
                                            <p class="title-slide-about-us" style="color:rgb(3,77,138);font-size:18px;">
                                                الاستشارات الاستثمارية
                                                الاستشارات العقارية&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>-
                                                تقييم الممتلكات
                                            </p></div>
                                        <div class="col"><img src="<?= url('/storage/general/about4.png') ?>" alt="about4.png"></div>
                                    </div>
                                </div>
                                <div class="container"><h1 class="oposite-2" style="font-size:40px;"
                                                           class="about-us-all-title">سما الإمارات، تجسيد للتميز</h1>
                                    <div class="row">
                                        <div class="col-md-auto"><img src="<?= url('/storage/general/we-ar.svg') ?>" class="we-img" alt="We.png"></div>
                                        <div class="col-md-6"><p>على الرغم من أن الجميع يسعى نحو التميز، قلة هم من ينطبق
                                                عليهم هذا الوصف حقًا!
                                                أمّا سما الإمارات فاستطاعت تسجيد مفهوم التميز تسجيداً واقعياً بفضل
                                                خدماتها العقارية الإستثنائية بحيث أصبحت الدليل الوحيد الذي يلبي تطلعات
                                                عملائها العالية.
                                                <br>&nbsp;
                                            </p>
                                            <p style="font-size:19px;font-weight:bolder;">سما الإمارات؟ تعطيك ما تستحق
                                                وأكثر!&nbsp;<br>&nbsp;</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="container">
                                    <h1 style="color:rgb(135,181,214);font-weight:bolder;">الخبرة والمعرفة</h1>
                                    <p>إنّ التنبؤ بالمستقبل أمر مستحيل بلا شك، إلا أن الخبرة الطويلة تجعل توقع ما سيحدث
                                        ممكنًا عبر تحديد أوجه التشابه ما بين الأحداث السابقة والحالية، وهذا ما تميزنا به
                                        خبرتنا الطويلة التي تمتد لأكثر من 20 عامًا في السوق العقاري، إذ أنها تمكننا من
                                        توقع المشاكل وتجنبها عبر الاستفادة من التجارب السابقة.</p>
                                </div>
                                <div class="container">
                                    <h1 style="color:rgb(135,181,214);font-weight:bolder;">خدمة على مدار الساعة</h1>
                                    <p>من جملة الميزات التي نتمتع بها ويحبها عملاؤنا هي إمكانية التواصل معنا في كل وقت
                                        وحين، فيمكن لعملائنا التواصل معنا متى ما أرادوا حتى نجيب على أسئلتهم ونزودهم
                                        بتفاصيل إضافية ليكونوا على دراية تامة بخدماتنا.</p>
                                </div>
                                <div class="container">
                                    <h1 style="color:rgb(135,181,214);font-weight:bolder;">
                                        المصداقيّة والأمانة</h1>
                                    <p>إنّ المصداقية والأمانة التي تتصف بهما شركة سما الإمارات ما هما إلا امتدادًا لصدق
                                        وأمانة خبرائنا الذين يعملون على خدمة العميل بنزاهة وتفانٍ، ويشهد على ذلك كل من
                                        سبق له الاستفادة من خدماتنا.</p></div>
                                <div class="container">
                                    <h1 style="color:rgb(135,181,214);font-weight:bolder;">الالتزام</h1>
                                    <p>يلتزم جميع موظفو سما الإمارات بأعلى المعايير المهنية، الأمر الذي ينعكس على جودة
                                        خدماتنا ويجعلنا رواد المجال العقاري.</p></div>
                                <div class="container-fluid marketing-ads-section" id="marketing-and-ads">
                                    <h1 style="color:rgb(255,255,255);text-align:center;">التسويق والإعلانات</h1>
                                    <section class="container marketing-text-box">
                                        <div class="row" style="background-color:rgb(255,255,255);">
                                            <?php
                                            if (app()->getLocale() == "ar")
                                                $url = "/ar/buy-properties";
                                            else
                                                $url = "/buy-properties";
                                            ?>
                                            <div class="col"><p style="font-size:20px;" class="slide-about-us-markting">
                                                    هل تبحث عن عقار في الإمارات العربية المتحدة ولا تعرف من أين تبدأ؟ لقد وصلت إلى المكان المناسب!.
                                                    ستساعدك سما الإمارات في كل خطوة حتى تتمكن من اتخاذ خيارك بكامل ثقة.
                                                    <br>
                                                    بينما نعمل معا للعثور على منزلك المستقبلي ، سنناقش ميزات كل إمارة ، وأفضل المواقع للاختيار من بينها ، وعوامل أخرى قد ترغب في أخذها في الاعتبار.
                                                    <br>
                                                    سنعمل بلا كلل حتى تصبح سعيدا أخيرا بمنزلك الجديد!
                                                    .</p>
                                                <p class="mt-4"><a
                                                        style="border-radius: 5px; border: 2px solid rgb(3, 77, 138); color: rgb(3, 77, 138); font-size: 24px; padding: 0.5% 3%; text-decoration: none;"
                                                        href="<?= url($url) ?>"
                                                        class="buy-font"><?php echo __("Buy your dream property!") ?></a>
                                                </p></div>
                                        </div>
                                    </section>
                                </div>
                                <div class="container text-center sama-logo-banner"><img
                                        src="<?= url('storage/general/path-5409.png')?>" alt="Path%205409.png">
                                </div>
                                <div class="container our-values">
                                    <section style="min-height:669px;">
                                        <h1 style="float:left;color:rgb(159,162,165);font-size:50px;font-weight:500;text-align:right;width:200px;">
                                            ما هي &nbsp;&nbsp;<br>قيمنا&nbsp;؟</h1>
                                        <div class="content_img">
                                            <img class="our-values-image-1" src="<?= url('/storage/about/Honsety.png')?>" alt="Component%2048%20–%201.png">
                                            <div class="text-what-value"> الأمانة </div>
                                            <span class="span1over span1over1" style="font-size:20px;height:400px;padding-left:60px;text-align:center;width:98%;">الصراحة والشفافية أساسان تقوم عليهما كافة خدماتنا.</span>
                                        </div>
                                        <div class="content_img2" style="width:380px;">
                                            <img class="our-values-image-2" src="<?= url('/storage/about/Accountability.png')?>" alt="Component%2049%20–%201.png">
                                            <div class="text-what-value-1"> المسئولية </div>
                                            <span class="span1over span4over4" style="font-size:20px;height:500px;padding-left:60px;text-align:center;width:98%;">نتحمّل مسؤوليّة كل ما نتخذه من قرارات ونقوم به من أفعال.</span>
                                        </div>
                                        <div class="content_img3" style="width:550px;">
                                            <img class="our-values-image-3" src="<?= url('/storage/about/Integrity.png')?>" alt="Component%2050%20–%201.png">
                                            <div class="text-what-value"> التكامل </div>
                                            <span class="span1over span3over3" style="font-size:20px;height:250px;padding-left:60px;text-align:center;width:89%;">
                                                لا يمكن لأي شركة عقارية تحقيق أي نجاح دون أن تتسم جميع خدماتها بالنزاهة.
                                            </span>
                                        </div>
                                        <div class="content_img4" style="width:400px;">
                                            <img class="our-values-image-4" src="<?= url('/storage/about/Value-centricity.png')?>" alt="Component%2051%20–%201.png">
                                            <div class="text-what-value-1"> المركزية القيمة </div>
                                            <span class="span1over span2over2"
                                                style="font-size:20px;height:333px;padding-left:60px;text-align:center;width:92%;">مهما كانت الظروف، سنبقى متمسكين بقيمنا ومبادئنا على الدوام.</span>
                                        </div>
                                        <p class="our-values-at-sama" style="font-size:23px;width:248px;">قيمنا
                                            نعرض عليكم فيما يلي بعضًا من القيم التي لا نحيد عنها في سما الإمارات
                                        </p></section>
                                </div>
                                <div class="container" id="our-team">
                                    <h2 style="color:rgb(3,77,138);font-size:40px;" class="about-us-all-title">فريقنا</h2>
                                    <p>يضم فريق شركة عقارات سما الإمارات خبراء موهوبين يملؤهم الشغف، إلى جانب المهندسين
                                        المحترفين ومساحي الأراض المتخصصين والاستشاريين المطّلعين وغيرهم الكثير. إليكم
                                        نبذة عن أعضاء فريق
                                        <strong>شركة عقارات سما الإمارات:</strong>:</p></div>
                                <div class="container">
                                    <div class="row" style="background-color:rgb(240,247,255);">
                                        <div class="col-md-4">
                                            <img src="<?= url("storage/general/Aref Ismail Al Khoori.jpg")   ?>"
                                                                   style="max-height:268.625px !important;max-width: 255px !important;border-radius: 30px !important;"
                                                                   alt="Small%20teaser.png"></div>
                                        <div class="col-md-8">
                                            <h1 class="about-us-title-person-team"> عارف إسماعيل الخوري</h1>
                                            <p><span>المدير التنفيذيّ:</span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>&nbsp;
                                            </p>
                                            <p>عارف إسماعيل خوري هو المدير التنفيذيّ لشركة عقارات سما الإمارات، وعلى
                                                مدار أكثر من 14 عامًا كان واحدًا من أكثر الشخصيات تأثيرًا في سوق العمل
                                                الإماراتي، فقد كان عضوًا في مجالس إدارة مختلف الشركات العقاريّة، وشركات
                                                التأمين والمؤسسات والبنوك الإسلاميّة.</p>
                                            <div>
                                                <p>
                                                    <button class="btn btn-link btn-sm read-more">أقرأ المزيد</button>
                                                </p>
                                                <div class="collapse"><p>يحمل عارف إسماعيل الخوري شهادة ماجستير في إدارة
                                                        الأعمال في تخصص المالية والمحاسبة، ويشغل حاليا منصب رئيس شركة
                                                        سما الإمارات العقارية وشركة سما للاستشارات المالية.</p></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="container team-section">
                                    <div class="row">
                                        <div class="col-md-6" style="text-align:center;"><p>

                                                <img class="our-time-img" src="<?= url("storage/general/hazem_mohamed.jpg") ?>"
                                                     alt="Small%20teaser2.png">
                                            </p>
                                            <div class="text-our-team">
                                                <h1 style="font-size:30px;" class="about-us-title-person-team">حازم محمد</h1>
                                                <p><span>وسيط عقاري</span>&nbsp;<br>&nbsp;</p></div>
                                        </div>
                                        <div class="col-md-6" style="text-align:center;"><p>
                                                <img class="our-time-img" src="<?= url("storage/general/karim_fayez.jpg") ?>"
                                                     alt="Small%20teaser3.png">
                                            </p>
                                            <div class="text-our-team">
                                                <h1 style="font-size:30px;" class="about-us-title-person-team">كريم
                                                    فايز</h1>
                                                <p><span>وسيط عقاري</span>&nbsp;<br>&nbsp;</p></div>
                                        </div>
                                        <div class="col-md-6" style="text-align:center;"><p>
                                                <img class="our-time-img" src="<?= url("storage/general/malika.jpg") ?>"
                                                     alt="Small%20teaser4.png">
                                            </p>
                                            <div class="text-our-team">
                                                <h1 style="font-size:30px;" class="about-us-title-person-team">
                                                    ملاك زيدي
                                                </h1>
                                                <p><span>وسيط عقاري</span>&nbsp;<br>&nbsp;</p></div>
                                        </div>
                                        <div class="col-md-6" style="text-align:center;"><p>
                                                <img class="our-time-img" src="<?= url("storage/general/soukaina.jpg") ?>"
                                                     alt="Small%20teaser4.png">
                                            </p>
                                            <div class="text-our-team">
                                                <h1 style="font-size:30px;" class="about-us-title-person-team">
                                                    سكينة كبيدا
                                                </h1>
                                                <p><span>وسيط عقاري</span>&nbsp;<br>&nbsp;</p></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="container sell-with-us sell-with-us-custom about-us-border"
                                     style="background-color:rgb(227,238,246);height:477px;">
                                    <div class="Buy-Rent"><h2
                                            style="color:rgb(20,138,200);font-size:40px;font-weight:bolder;"
                                            class="about-us-all-title">هل تريد شراء/ ايجار عقار في الإمارات؟ </h2>
                                        <p style="font-size:18px;" class="about-us-content-1">الآن فرصتك لامتلاك عقار أحلامك في الإمارات! اختر من بين عقاراتنا المختلفة العقار المثالي الذي يناسب أسلوب حياتك وعائلتك. سما الإمارات تساعدك لاختيار الأفضل!</p>
                                        <p style="font-size:30px;" class="just about-us-content-1">انس الإشارات عالطريق! خلك مركز مع سما الإمارات بتوصل لبيت الأحلام</p></div>

                                    <?php


                                    if (app()->getLocale() == "ar") {
                                        $urlSale = "/ar/buy-properties";
                                        $urlRent = "/ar/rent-properties";
                                    } else {
                                        $urlSale = "/buy-properties";
                                        $urlRent = "/rent-properties";
                                    }


                                    ?>

                                    <div class="mt-4">
                                        <div class="col mt-4" style="text-align:center;"><a
                                                style="border-radius: 5px; border: 2px solid rgb(4, 78, 138); color: rgb(4, 78, 138); padding: 0.5% 7%; text-align: center;"
                                                href="<?= url($urlSale) ?>"><?php echo __("Buy now") ?></a></div>
                                        <div class="col mt-4" style="text-align:center;"><a
                                                style="border-radius: 5px; border: 2px solid rgb(4, 78, 138); color: rgb(4, 78, 138); padding: 0.5% 7%; text-align: center;"
                                                href="<?= url($urlRent) ?>"><?php echo __("Rent now") ?></a></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <br>
            <br>
        </div>
    </div>
<?php } ?>
<?php echo Theme::partial('footer-about-us') ?>




