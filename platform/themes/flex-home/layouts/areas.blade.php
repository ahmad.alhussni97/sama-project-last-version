{!! Theme::partial('header') !!}
@if(app()->getLocale()=="en")
    <div id="app">
        <div class="bgarea hidden-xs">
            <div class="description description-custom">
                <div class="container-fluid w90 color-blue">

                    {!! Theme::partial('breadcrumb') !!}
                </div>

            </div>
        </div>
        <div id="is-areas">
            <div class="container">
                <div style="position: relative;width: 100%;height: 600px;">
                    <a href="{{url('/rent-properties?location=Fujairah')}}" style="position: absolute;z-index: 1;top: 100px;">
                        <img src="{{url('/storage/area-new/areas-name/Fujairah.png')}}">
                        <div class="show-text-area"> Fujairah-> </div>
                    </a>
                    <a href="{{url('/buy-properties?location=Abu Dhabi')}}" style="position: absolute;z-index: 0;top: 50px;left: 277px;">
                        <img src="{{url('/storage/area-new/areas-name/Abu Dhabi.png')}}">
                        <div class="show-text-area"> Abu Dhabi-> </div>
                    </a>
                    <a href="{{url('/buy-properties?location=Ajman')}}" style="position: absolute;z-index: 2;top: 20px;left: 700px;">
                        <img src="{{url('/storage/area-new/areas-name/Ajman.png')}}">
                        <div class="show-text-area"> Ajman-> </div>
                    </a>
                    <a href="{{url('/rent-properties?location=Dubai')}}" style="position: absolute;z-index: 1;top: 219px;left: 557px;">
                        <img src="{{url('/storage/area-new/areas-name/Dubai.png')}}">
                        <div class="show-text-area"> Dubai-> </div>
                    </a>
                    <a href="{{url('/buy-properties?location=Sharjah')}}" style="position: absolute;z-index: 4;top: 326px;left: 208px;">
                        <img src="{{url('/storage/area-new/areas-name/Sharjah.png')}}">
                        <div class="show-text-area"> Sharjah-></div>
                    </a>
                    <a href="{{url('/buy-properties?location=Umm Al Quwain')}}" style="position: absolute;z-index: 2;top: 297px;left: -38px;">
                        <img src="{{url('/storage/area-new/areas-name/Umm Alquwain.png')}}">
                        <div class="show-text-area"> Umm Al Quwain-></div>
                    </a>
                    <a href="{{url('/buy-properties?location=Ras Al Khaimah')}}" style="position: absolute;z-index: 0;top: 219px;left: 750px;">
                        <img src="{{url('/storage/area-new/areas-name/Ras Alkhaimah.png')}}">
                        <div class="show-text-area"> Ras Al Khaimah-></div>
                    </a>
                    <a href="{{url('/buy-properties?location=Cairo')}}" style="position: absolute;z-index: 0;top: 400px;left: 750px;">
                        <img src="{{url('/storage/area-new/areas-name/Cairo.jpeg')}}" style="border-radius: 20%;border: 20px solid #FFFFFF;">
                        <div class="show-text-area"> Cairo-></div>
                    </a>
                </div>
            </div>
            <div class="container container-margin" style="padding-top:60px;">
                <h1 style="color: rgb(0,0,0);">What Can You Buy in UAE?</h1>
                <div class="row">
                    <div class="col-auto">
                        <p style="font-size: 284px;font-weight: 200;line-height: 218px;color: #9fc4de;">?</p>
                    </div>
                    <div class="col">
                        <p>The real question is not “What can you buy in UAE?” but rather “What can you NOT buy in UAE?”. Real estates in UAE are one of the best options when it comes to investing and making the most out of the owned property. The United Arab Emirates is one of the countries that contain a large number of real estate properties that can be bought or rented. Emirates properties can include real estates in Dubai, real estates in Abu Dhabi, real estates in Ajman, and many other options.</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <h2>Why Shall You Buy in UAE?</h2>
                    </div>
                </div>
            </div>
            <div class="container" style="padding-top: 34px;">
                <h1 style="color: #87B5D6;font-weight: bolder;">Safety</h1>
                <p style="padding-top: 38px;">The United Arab Emirates is considered to be one of the countries where crime rates are very low and living in it means forever living in one of the safest countries in the whole world. Your property is safe, your future is safe, YOU are safe. This means that you can choose from real estates in Dubai or in other locations. Safety is taking over the whole country.</p>
            </div>
            <div class="container" style="padding-top: 34px;">
                <h1 style="color: #87B5D6;font-weight: bolder;">Somewhat Cheap</h1>
                <p style="padding-top: 38px;">Unlike what many believe to be true, real estates in UAE are not as expensive as other cosmopolitan hubs. This means that buying in UAE is considered to be cheap and affordable mainly because of the wide selection of properties in the country. Dubai property is one of the options that buyers and investors look for.</p>
            </div>
            <div class="container" style="padding-top: 34px;">
                <h1 style="color: #87B5D6;font-weight: bolder;">High Rent</h1>
                <p style="padding-top: 38px;">Making the decision of buying real estates in UAE means that owners will benefit from renting out their properties since rent is considered to be high in the United Arab Emirates. This is the concretized definition of wise thinking. As real estate experts, we highly advise our customers to think outside the box; buy and rent out. No one has ever regretted it.</p>
            </div>
            <div class="container" style="padding-top: 34px;">
                <h1 style="color: #87B5D6;font-weight: bolder;">Great for investment &amp; business purposes</h1>
                <p style="padding-top: 38px;">Real estates in UAE are great for investment and business purposes, especially that it includes cosmopolitan hubs such as Dubai. It also welcomes millions of tourists each year, making it one of the most visited countries in the whole world. Dubai real estate is a very good option to start growing and targeting the right audience in the right country.</p>
            </div>
            <div class="container" style="padding-top: 34px;">
                <h1 style="color: #87B5D6;font-weight: bolder;">Excellent Social Life</h1>
                <p style="padding-top: 38px;">To buy property in Dubai or anywhere else in the United Arab Emirates, is to benefit from the exceptional social life. The lifestyle UAE is one of a kind, since it is the true definition of luxury. One can have dinner, enjoy a day at the beach, or even spend all day shopping. Nightlife in the United Arab Emirates, especially in Dubai, is one of the best features of the country.</p>
            </div>
            <div class="container" style="padding-top: 34px;">
                <h1 style="color: #87B5D6;font-weight: bolder;">No Taxes</h1>
                <p style="padding-top: 38px;">The investor-friendly country offers buyers the chance to work, live, and own real estates in UAE without having to pay taxes. Everything you earn is entirely yours to keep. This is another great reason that makes buying real estates in UAE one of the best options. Living tax-free is a dream itself.</p>
            </div>
            <div class="container" style="padding-top: 34px;">
                <h1 style="color: #87B5D6;font-weight: bolder;">Cheaper Than Rent</h1>
                <p style="padding-top: 38px;">Since rent is considered to be somewhat expensive in the United Arab Emirates,
                    buying real estates in UAE is definitely better than renting a property to live in. Think about all the
                    money you can save on the long run. It’s all about perspective anyway. And good math, obviously. Buy,
                    rent out, live the luxurious life you have always dreamt of.</p>
            </div>
            <div class="container" style="padding-top: 34px;">
                <h1 style="color: #87B5D6;font-weight: bolder;">Strategic Location</h1>
                <p style="padding-top: 38px;">In the United Arab Emirates, everything falls into place. The strategic
                    location of this country is one of its best features. Thus, all it takes is a 6-hour flight to get to
                    the majority of the destinations. Not to mention the cosmopolitan hubs it includes such as Dubai and Abu
                    Dhabi, making the UAE a once-in-a-lifetime opportunity.</p>
            </div>
            <div class="container" style="margin-top: 76px;margin-bottom: 76px;">
                <h2 class="text-center">Who Can Buy/ Rent in UAE?</h2>
                <div class="row mt-5">
                    <div class="col-md-3 img__wrap col-sm-6">
                        <img src="{{asset('storage/area-new/Image-3.png')}}" class="img-circle">
                        <div class="show-text"> Employees</div>
                        <div class="overlay">
                          <span>
                        Whether they are local or foreign employees, they too can rend a flat, a house or an apartment
                         To live in that is close to work. They can also buy a property in UAE.
                          </span>
                        </div>
                    </div>
                    <div class="col-md-3 img__wrap col-sm-6">
                        <img src="{{asset('storage/area-new/Image-2.png')}}" class="img-thumbnail">
                        <div class="show-text-type-2"> Students</div>
                        <div class="overlay-type-2">
                          <span>
                      Student who move in to UAE can easily rent a flat or an apartment in UAE include properties for
                      Students to rent that are affordable and close to their university campus.
                          </span>
                        </div>
                    </div>
                    <div class="col-md-3 img__wrap col-sm-6">
                        <img src="{{asset('storage/area-new/Image-1.png')}}" class="img-circle">
                        <div class="show-text"> Foreigners</div>
                        <div class="overlay">
                          <span>
                         Foreigners can buy real estates in UAE in the areas that are designated as freehold. They can
                         Then rent out the property or simply live in it. Both options are valid and both are beneficial.
                          </span>
                        </div>
                    </div>
                    <div class="col-md-3 img__wrap col-sm-6">
                        <img src="{{asset('storage/area-new/Image.png')}}" class="img-thumbnail">
                        <div class="show-text-type-2"> Business Owners</div>
                        <div class="overlay-type-2">
                          <span>
                      Foreigners can buy real estates in UAE in the areas that are designated as freehold. They can
                       Then rent out the property or simply live in it. Both options are valid and both are beneficial.
                          </span>
                        </div>
                    </div>
                </div>
                <div class="row mt-5">
                    <div class="col-md-3 img__wrap col-sm-6">
                        <img src="{{asset('storage/area-new/Image-6.png')}}" class="img-circle">
                        <div class="show-text"> Investors</div>
                        <div class="overlay">
                          <span>
                   Whether Dubai real estate or other real estates in UAE, investing in the United Arab Emirates is one of the best choices and investors can indeed benefit  from the advantages of the market
                          </span>
                        </div>
                    </div>
                    <div class="col-md-3 img__wrap col-sm-6">
                        <img src="{{asset('storage/area-new/Image-4.png')}}" class="img-thumbnail">
                        <div class="show-text-type-2"> Tourists</div>
                        <div class="overlay-type-2">
                          <span>
                         Tourists can rent a flat an apartment a house or anything they want if they are visiting the United Arab Emirates to discover one of the most beautiful and luxurious countries ever
                          </span>
                        </div>
                    </div>
                    <div class="col-md-3 img__wrap col-sm-6">
                        <img src="{{asset('storage/area-new/Image-7.png')}}" class="img-circle">
                        <div class="show-text"> Residents</div>
                        <div class="overlay">
                          <span>
                     Anyone can own or rent a property in the United Arab Emirates no matter the age.
                     Therefore , all residents are allowed to buy a nice apartment or temporarily rent a flat.
                          </span>
                        </div>
                    </div>
                    <div class="col-md-3 img__wrap col-sm-6">
                        <img src="{{asset('storage/area-new/Image-5.png')}}" class="img-thumbnail">
                        <div class="show-text-type-2"> Newlyweds</div>
                        <div class="overlay-type-2">
                          <span>
                    The best for the last! Newlyweds can start the journey just right by renting or buying real estates
                     In UAE. Starting a family with your partner has never been this perfect.
                          </span>
                        </div>
                    </div>
                </div>

            </div>
            <h2 class="text-center">How Can You Buy/ Rent in UAE?</h2>
            <div class="container text-center" style="margin-top: 81px;margin-bottom: 104px;position: relative">
                <img src="{{url('/storage/area-new/How Can Buy Rent in UAE/engish.png')}}">
                <div class="content-img-1"> <b>1.</b> <br> <span>Fill up the basic <br>   details.</span></div>
                <div class="content-img-2"> <b>2.</b> <br> <span>Property Review<br> Process.</span></div>
                <div class="content-img-3"> <b>3.</b> <br> <span>Our team will contact you <br> if we need any further <br> information.</span></div>
                <div class="content-img-4"> <b>4.</b> <br> <span>Your property <br> goes live.</span></div>
            </div>

            <div class="container"
                 style="background: #e3eef6;border-radius: 20px;padding-top: 80px;padding-bottom: 50px;margin-bottom: 84px;">
                <div style="padding-left: 144px;" class="Buy-Rent">
                    <h2 style="color: #148AC8;font-weight: bolder;font-size: 40px;" class="title-area-buy">Buy/ Rent in UAE Now!</h2>
                    <p style="padding-left: 71px;font-size: 18px;padding-top: 40px;padding-right: 142px;">How do you feel
                        being very close to turning your dream into reality? How do you feel about the milestone you are
                        about to reach? We get it, buying real estates in UAE or renting real estates in UAE might be
                        overwhelming, especially if you do not have enough insights, but with the help of professionals,
                        it’s a cinch! And remember, better late than never, but late is never better. So stop dreaming and
                        start doing!</p>
                    <p style="padding-left: 71px;font-size: 30px;padding-top: 40px;padding-right: 142px;" class="just">Just remember,
                        lights will NOT guide you home, but SAMA Emirates Properties definitely will!</p>
                </div>
                <div class="row" style="margin-top: 41px;">
                    <div class="col" style="text-align: center;">
                        @php
                            if(app()->getLocale() =="ar")
                                  $url="/ar/buy-properties";
                            else
                                  $url="/buy-properties";
                        @endphp
                        <a href="{{url($url)}}"
                           class="your-dream"style="margin: 10px auto;text-align: center;padding:20px;border:#044e8a solid 1px; color:#044e8a;">{{__("Buy your dream house now!")}}</a></div>
                </div>
            </div>
            {{--        {!! Theme::content() !!}--}}
        </div>
    </div>
@else
<div id="app">
    <div class="bgarea hidden-xs">
        <div class="description description-custom">
            <div class="container-fluid w90 color-blue">

                {!! Theme::partial('breadcrumb') !!}
            </div>

        </div>
    </div>
    <div id="is-areas">
        <div class="container">
            <div style="position: relative;width: 100%;height: 600px;">
                <a href="{{url('/ar/rent-properties?location=Fujairah')}}" style="position: absolute;z-index: 1;top: 100px;">
                    <img src="{{url('/storage/area-new/areas-name/Fujairah.png')}}">
                    <div class="show-text-area"> الفجيرة -></div>
                </a>
                <a href="{{url('/ar/buy-properties?location=Abu Dhabi')}}" style="position: absolute;z-index: 0;top: 50px;left: 277px;">
                    <img src="{{url('/storage/area-new/areas-name/Abu Dhabi.png')}}">
                    <div class="show-text-area"> أبو ظبي -> </div>
                </a>
                <a href="{{url('/ar/buy-properties?location=Ajman')}}" style="position: absolute;z-index: 2;top: 20px;left: 700px;">
                    <img src="{{url('/storage/area-new/areas-name/Ajman.png')}}">
                    <div class="show-text-area"> عجمان -> </div>
                </a>
                <a href="{{url('/ar/rent-properties?location=Dubai')}}" style="position: absolute;z-index: 1;top: 219px;left: 557px;">
                    <img src="{{url('/storage/area-new/areas-name/Dubai.png')}}">
                    <div class="show-text-area"> دبي -> </div>
                </a>
                <a href="{{url('/ar/buy-properties?location=Sharjah')}}" style="position: absolute;z-index: 4;top: 326px;left: 208px;">
                    <img src="{{url('/storage/area-new/areas-name/Sharjah.png')}}">
                    <div class="show-text-area"> الشارقة -> </div>
                </a>
                <a href="{{url('/ar/buy-properties?location=Umm Al Quwain')}}" style="position: absolute;z-index: 2;top: 297px;left: -38px;">
                    <img src="{{url('/storage/area-new/areas-name/Umm Alquwain.png')}}">
                    <div class="show-text-area">أم القيون -> </div>
                </a>
                <a href="{{url('/ar/buy-properties?location=Ras Al Khaimah')}}" style="position: absolute;z-index: 0;top: 219px;left: 750px;">
                    <img src="{{url('/storage/area-new/areas-name/Ras Alkhaimah.png')}}">
                    <div class="show-text-area"> رأس الخيمة -> </div>
                </a>

                <a href="{{url('/buy-properties?location=Cairo')}}" style="position: absolute;z-index: 0;top: 400px;left: 750px;">
                    <img src="{{url('/storage/area-new/areas-name/Cairo.jpeg')}}" style="border-radius: 20%;border: 20px solid #FFFFFF;">
                    <div class="show-text-area"> مصر -></div>
                </a>

            </div>
        </div>
        <div class="container container-margin" style="padding-top:60px;">
            <h1 style="color: rgb(0,0,0);">ماذا يمكنك أن تشتري في الإمارات العربيّة المتّحدة؟</h1>
            <div class="row">
                <div class="col-auto">
                    <p style="font-size: 284px;font-weight: 200;line-height: 218px;color: #9fc4de;">؟</p>
                </div>
                <div class="col">
                    <p>لا يوجد ما لا تستطيع شراءه في دولة الإمارات التي باتت وجهةً عالمية رائدةً في كافة المجالات وعلى جميع الأصعدة، ولكن، يبقى قطاع العقارات في الإمارات الاستثمار الأفضل بلا منازع. ولا يخفى على أحد أن دولة الإمارات غنية بالمنشآت االعقارية المتميزة المتاحة للشراء والاستئجار وهنا تتسع خياراتك مرة أخرى، فالعقارات في الإمارات تمتد لتشمل كافة أراضيها ابتداءً من العقارات في دبي والعقارات في الشارقة وحتى العقارات في عجمان وغيرها من الإمارات، الأمر الذي يكفل نجاحك في تحقيق أهدافك الاستثمارية مهما كانت.</p>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <h2>شراء عقار في دولة الإمارات العربيّة المتّحدة: أبرز المنافع</h2>
                </div>
            </div>
        </div>
        <div class="container" style="padding-top: 34px;">
            <h1 style="color: #87B5D6;font-weight: bolder;">الأمان</h1>
            <p style="padding-top: 38px;">
                سمِّ دولة أكثر اماناً من دولة الإمارات العربية المتحدة. سمِّ دولة يتجول فيها الناس في منتصف الليل في الشوارع دون خوف أو قلق! لما التردد؟
                مع انخفاض معدل الجريمة نسبةً إلى الدول العربية والأجنبية الأخرى، فإن أي إمارة من الإمارات العربية المتحدة التي ستختارها لشراء عقارات في الإمارات ستكون خياراً آمناً لعملك أو سكنك الجديد!
                اشتر عقارك اليوم وامنح عائلتك الآمان لمدى الحياة، سيشكرونك على ذلك.
            </p>
        </div>
        <div class="container" style="padding-top: 34px;">
            <h1 style="color: #87B5D6;font-weight: bolder;">الأسعار المعقولة</h1>
            <p style="padding-top: 38px;">على عكس ما يعتقده الكثيرون، ليست الإمارات العربية المتحدة منزل الأغنياء فقط. إذ تقدم خيارات كثيرة لشراء العقارات في الإمارات بأسعار معقولة بل حتى أكثر من الأسعار السارية في العديد من مناطق الجذب حول العالم!</p>
            <p style="padding-top: 38px;">نعم صدّق أو لا تصدقّ، ستجد أن العقارات في دبي تلقى رواجًا كبيرًا بين المستثمرين والراغبين بشراء عقارات في الإمارات. إذا كنت تفكر في شراء عقار في الإمارات العربية المتحدة، فلا تدع المخاوف بشأن التكلفة تردعك - من المؤكد أن هناك ما يناسب ميزانيتك!</p>
        </div>
        <div class="container" style="padding-top: 34px;">
            <h1 style="color: #87B5D6;font-weight: bolder;">الموقع الاستراتيجيّ</h1>
            <p style="padding-top: 38px;">أنت تعلم أكثر منّا أن الإمارات العربيّة المتّحدة تتميز بموقعها الاستراتيجي الذي قد تحلم بها الدول العربية، ويشهد على ذلك مطار دبي، أحد أكثر المطارات ازدحامًا في العالم! ما يجعل شراء العقارات في الإمارات فرصة ذهبية لا تتكرر واستثمارًا حكيمًا في موقع استثنائي لا يحتاج إلى تفكير أو مغامرة.</p>
        </div>
        <div class="container" style="padding-top: 34px;">
            <h1 style="color: #87B5D6;font-weight: bolder;">خيار صائب للاستثمار</h1>
            <p style="padding-top: 38px;">
                لماذا تجازف بالاستثمار في ما ليس ثابتاً أو مضموناً بينما يمكنك الاستثمار في مجال واحد وأكيد يضمن لك عوائد على استثمار هائلة؟ إنّ شراء العقارات في الإمارات استثمار صائب في جميع الأوقات!
                إن سوق العقارات في الإمارات مهما شهد من اضطرابات يعود أقوى من قبل، ما يجعل شراء العقارات في دبي الخيار الأمثل للمستثمرين وأصحاب الأعمال الراغبين بتطوير أعمالهم وتحقيق عوائد على استثمار مربحة!</p>
        </div>
        <div class="container" style="padding-top: 34px;">
            <h1 style="color: #87B5D6;font-weight: bolder;"> الحياة الاجتماعيّة المزدهرة</h1>
            <p style="padding-top: 38px;">لن تشعر بالملل أبدا في الإمارات العربية المتحدة لأن هناك مكاناً جديداً لزيارته أو نشاطاً مثيراً للقيام به كل يوم وكلّ لحظة! وهو مكان رائع للقاء الناس من جميع أنحاء العالم!
                إن شراء العقارات في الإمارات، وفي إمارة دبي على وجه الخصوص، يوفر لك نمط حياة فريد ومليء بالحياة حيث تارةً تجد نفسك تتزلج على الجليد صباحًا، وفي الظهر تتسبح قليلاً وتارةً تخيم في الصحراء ليلًا. </p>
        </div>
        <div class="container" style="padding-top: 34px;">
            <h1 style="color: #87B5D6;font-weight: bolder;">الإعفاء من الضرائب</h1>
            <p style="padding-top: 38px;">هل سمعت سكان الدول الأجنبية يشكون من الضرائب التي لا تطاق؟ إن أحد المزايا الإضافية لدولة الإمارات العربية المتحدة هي أن شراء العقارات في الإمارات واستثمارها لا يترتب عليها أي ضرائب! حتى معاشك التي تتحصله كل شهر هو لك بالكامل! الأمر الذي سيتيح لك تحقيق الاستفادة القصوة من الدخل والإنفاق مقابل الجهد والتعب الذذي تستثمره.</p>
        </div>
        <div class="container" style="padding-top: 34px;">
            <h1 style="color: #87B5D6;font-weight: bolder;">رتفاع قيمة الإيجارات</h1>
            <p style="padding-top: 38px;">إذا كنت تتطلع إلى استثمار أموالك في عقار، نقدّم لك نصيحة من خبرتنا الطويلة في مجال العقارات في الإمارات: اشترِ عقار في الإمارات واستثمره في الإيجار عبر تأجيره شهرياً أو سنوياً!  إنها نصيحة لم يندم على اتباعها أحدٌ قط!</p>
        </div>
        <div class="container" style="padding-top: 34px;">
            <h1 style="color: #87B5D6;font-weight: bolder;">  أرخص من الايجار!</h1>
            <p style="padding-top: 38px;">مع ارتفاع قيمة الإيجارات واعتدال أسعار شراء العقارات في الإمارات، لا يختلف اثنان على أن الخيار الأنسب للتوفير على المدى البعيد يتجلى بالاستثمار في شراء عقار عوضًا عن استئجاره أو شراء عقار في دبي وعرضه للإيجار والاستفادة من فروقات أسعار الإيجار بين منطقة وأخرى.
                استثمر في عقارات في الإمارات اليوم بشرائها وتأجيرها واستمتع بالحياة المترفة التي يحلم بها الجميع!</p>
        </div>
        <div class="container" style="margin-top: 76px;margin-bottom: 76px;">
            <h2 class="text-center">يمكنك استئجار/شراء العقارات في الإمارات العربيّة المتّحدة إن كنت أحد الفئات التالية:</h2>
            <div class="row mt-5">
                <div class="col-md-3 img__wrap col-sm-6">
                    <img src="{{asset('storage/area-new/Image-3.png')}}" class="img-circle">
                    <div class="show-text"> الطلّاب</div>
                    <div class="overlay">
                          <span>
               من أزعج الأمور البحث عن استوديو أو شقة صغيرة للإيجار… أليس كذلك أيها الطّلاب؟ خصوصاً وسط توترات العام الدراسي ومشاكلها…
                          </span>
                        <span>
                            يستطيع الطلاب في الإمارات العربية المتحدة اليوم استئجار العقارات في الإمارات بين استوديو أو شقة مخصصة للطلاب والتي تتوفر بأسعار يسيرة وتقع قرب أماكن الدراسة.
                        </span>
                    </div>
                </div>
                <div class="col-md-3 img__wrap col-sm-6">
                    <img src="{{asset('storage/area-new/Image-2.png')}}" class="img-thumbnail">
                    <div class="show-text-type-2"> الموظّفون </div>
                    <div class="overlay-type-2">
                          <span>
                    يمكن للموظّفين شراء العقارات في الإمارات أو استئجار شقّة أو منزل بالقرب من مكان عملهم. والأفضل؟ لن يتضروا إلى انفاق أكثر من نصف معاشاتهم على الإيجار!
                          </span>
                    </div>
                </div>
                <div class="col-md-3 img__wrap col-sm-6">
                    <img src="{{asset('storage/area-new/Image-1.png')}}" class="img-circle">
                    <div class="show-text"> أصحاب الأعمال</div>
                    <div class="overlay">
                          <span>
        خبر سار لرواد الأعمال وأصحاب المصالح! لن تجدوا مثل الإمارات لتطوير أعمالكم. أمامكم الخيارات ما بين استئجار العقارات في الإمارات أو شرائها سواء كان ذلك بهدف السكن في رخاء، العمل، أو حتى الاستثمار! اختاروا بين العقارات في دبي،  العقارات في أبو ظبي ،العقارات في الشارقة،  العقارات في راس الخيمة وغيرها من العقارات في الإمارات.
                          </span>
                    </div>
                </div>
                <div class="col-md-3 img__wrap col-sm-6">
                    <img src="{{asset('storage/area-new/Image.png')}}" class="img-thumbnail">
                    <div class="show-text-type-2">الأجانب</div>
                    <div class="overlay-type-2">
                          <span>
            غالباً ما يسألنا الناس السؤال التالي: هل يستطيع الأجانب شراء عقارات في الإمارات؟
                          </span>
                        <span>
                            نعم! يستطيع الأجانب شراء عقارات في الإمارات في مناطق التملك الحر، والاختيار ما بين الشراء والسكن أو الشراء وعرض العقار للإيجار بكل سهولة!
                        </span>
                    </div>
                </div>
            </div>
            <div class="row mt-5">
                <div class="col-md-3 img__wrap col-sm-6">
                    <img src="{{asset('storage/area-new/Image-6.png')}}" class="img-circle">
                    <div class="show-text"> المستثمرون</div>
                    <div class="overlay">
                          <span>
                   انتباه.. فرصةً ذهبية للمستثمرين!! توفّر العقارات في الإمارات ومنها العقارات في دبي للراغبين بالاستفادة من السوق العقاري المزدهر عوائد على الاستثمار أشبه بالحلم!
                          </span>
                    </div>
                </div>
                <div class="col-md-3 img__wrap col-sm-6">
                    <img src="{{asset('storage/area-new/Image-4.png')}}" class="img-thumbnail">
                    <div class="show-text-type-2"> السوّاح </div>
                    <div class="overlay-type-2">
                          <span>
                        الطائرة على وشك الهبوط... أهلاً بكم في إحدى أروع الوجهات السياحية في العالم!
لدى زيارتك هذه الدولة الغنية بالمرافق الترفيهية والمعالم السياحية الجاذبة، يمكنك استئجار شقة أو منزل أو استئجار فيلا أو أي عقار من العقارات في الإمارات لاستكشافها عن كثب!
                          </span>
                    </div>
                </div>
                <div class="col-md-3 img__wrap col-sm-6">
                    <img src="{{asset('storage/area-new/Image-7.png')}}" class="img-circle">
                    <div class="show-text"> المقيمون</div>
                    <div class="overlay">
                          <span>
                    إذا استفاد الأجانب والسيّاح، فكيف المقيمين؟ خيارات استئجار أو شراء عقارات في الإمارات متاحة بالكامل أمام جميع المقيمين، ولا شروط تقيدهم من التنعمّ بالأمان والاستقرار والرفاهية التي لا تبخل بها دولة الإمارات على سكانها.
                          </span>
                    </div>
                </div>
                <div class="col-md-3 img__wrap col-sm-6">
                    <img src="{{asset('storage/area-new/Image-5.png')}}" class="img-thumbnail">
                    <div class="show-text-type-2"> المتزوجون حديثًا</div>
                    <div class="overlay-type-2">
                          <span>
                     أخيرًا وليس آخرًا! لا تكتفي بشهر العسل في الإمارات! ابدأ حياتك الزوجية وحقق أحلامك ببناء أسرة مثالية في منزل مثالي، وابحث في خياراتك من استئجار إلى شراء عقارات في الإمارات بكافة الأحجام وكلّ ما تتخيله من مواصفات!
                          </span>
                    </div>
                </div>
            </div>

        </div>
        <h2 class="text-center">طريقة استئجار/شراء عقارات في الإمارات</h2>
        <div class="container text-center" style="margin-top: 81px;margin-bottom: 104px;position: relative">
            <img src="{{url('/storage/area-new/How Can Buy Rent in UAE/arabic.png')}}">
            <div class="content-img-1"> <b>1.</b> <br> <span> عبئ المعلومات  <br>الاساسية.</span></div>
            <div class="content-img-2"> <b>2.</b> <br> <span>عملية مراجعة <br> العقار.</span></div>
            <div class="content-img-3"> <b>3.</b> <br> <span>سيتواصل معك فريقنا<br> اذا احتجنا معلومات<br>أخرى.</span></div>
            <div class="content-img-4"> <b>4.</b> <br> <span>عقارك <br>قيد النشر.</span></div>
        </div>
        <div class="container"
             style="background: #e3eef6;border-radius: 20px;padding-top: 80px;padding-bottom: 50px;margin-bottom: 84px;">
            <div style="padding-left: 144px;" class="Buy-Rent">
                <h2 style="color: #148AC8;font-weight: bolder;font-size: 40px;" class="title-area-buy">اشتر/استأجر في الإمارات العربيّة المتّحدة الآن!</h2>
                <p style="padding-left: 71px;font-size: 18px;padding-top: 40px;padding-right: 142px;">
                    يُعد شراء عقار من الخطوات المحورية في الحياة والتي تتطلب التفكير والتخطيط وحساب جميع الخيارات، أما الاستئجار فيعد أكثر صعوبة لا سيما مع كثرة الخيارات المتاحة وتنوعها، لكن هذه المهمة تسهل بمساعدة خبراء يوفّرون المعلومة الدقيقة والخيارات الأفضل بالأسعار الأنسب، معنا، حلمك يتحقق واستثمارك يزدهر. اطلب مساعدتنا، لا لتأجيل الأحلام، نعم لتحقيقها!
                </p>
                <p style="padding-left: 71px;font-size: 30px;padding-top: 40px;padding-right: 142px;" class="just">انس الإشارات عالطريق! خلك مركز مع سما الإمارات بتوصل لبيت الأحلام</p>
            </div>
            <div class="row" style="margin-top: 41px;">
                <div class="col" style="text-align: center;">
                    @php
                        if(app()->getLocale() =="ar")
                              $url="/ar/buy-properties";
                        else
                              $url="/buy-properties";
                    @endphp
                    <a href="{{url($url)}}"
                     class="your-dream"style="margin: 10px auto;text-align: center;padding:20px;border:#044e8a solid 1px; color:#044e8a;">{{__("Buy your dream house now!")}}</a></div>
            </div>
        </div>
{{--        {!! Theme::content() !!}--}}
    </div>
</div>
@endif
{!! Theme::partial('footer') !!}

