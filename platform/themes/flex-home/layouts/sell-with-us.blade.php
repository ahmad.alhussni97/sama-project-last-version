{!! Theme::partial('header') !!}

<div id="app">

    <section>
        <div class="bgheadproject2 hidden-xs about-us-bg"
             style="background: url('/storage/general/mask-group-39-min.jpg')">
            <div class="description description-custom">
                <div class="container-fluid w90">
                    {!! Theme::partial('breadcrumb') !!}
                </div>
            </div>
            @php
                $string = SeoHelper::getTitle();
                $last_word_start = strrpos($string, ' ') + 1; // +1 so we don't include the space in our result
                $last_word = substr($string, $last_word_start);
                $string=trim($string, $last_word);
            @endphp
            <div class="container container_project">
                <h1 class="text-left panel-sell-with-us">{{ $string }}<b> {{ $last_word }} </b></h1>
                <p class="text-left">{{ theme_option('properties_description') }}</p>
            </div>
        </div>
    </section>

    @if (theme_option('show_map_on_properties_page', 'yes') == 'yes')
        <script id="traffic-popup-map-template" type="text/x-custom-template">
        {!! Theme::partial('real-estate.properties.map', ['property' => get_object_property_map()]) !!}
        </script>
    @endif

    <section class="display-none">
        <div class="big-title-sell-with-us">
            <p class="color_black font_weight_inherit">Fill out the form and our specialists will contact you!</p>
        </div>
        <div class="container">
            <form id="form-sell-with-us">
                <div class="form-group">
                    <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" id="customRadioInline1" name="customRadioInline1"
                               class="custom-control-input">
                        <label class="custom-control-label" for="customRadioInline1">Mr.</label>
                    </div>
                    <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" id="customRadioInline2" name="customRadioInline1"
                               class="custom-control-input">
                        <label class="custom-control-label" for="customRadioInline2">Mrs.</label>
                    </div>
                </div>
                <div class="form-row">
                    <div class="col-md-6 mb-3">
                        <label for="validationDefault01">First name*</label>
                        <input type="text" class="form-control sell-input" id="validationDefault01"
                               placeholder="First name"
                               value="Mark" required>
                    </div>
                    <div class="col-md-6 mb-3">
                        <label for="validationDefault02">Last name*</label>
                        <input type="text" class="form-control sell-input" id="validationDefault02"
                               placeholder="Last name"
                               value="Otto" required>
                    </div>
                </div>
                <div class="form-row">
                    <div class="col-md-6 mb-3">
                        <label for="validationDefault03">E-Mail*</label>
                        <input type="text" class="form-control sell-input" id="validationDefault03" placeholder="City"
                               required>
                    </div>
                    <div class="col-md-1 mb-3">
                        <label for="validationDefault04">Phone</label>
                        <input type="text" class="form-control sell-input" id="validationDefault04" placeholder="State"
                               required>
                    </div>
                    <div class="col-md-5 phone-input">
                        <input type="text" class="form-control sell-input" id="validationDefault04" placeholder="State"
                               required>
                    </div>

                </div>
                <div class="form-row">
                    <div class="col-md-6 mb-3">
                        <label for="validationDefault01">Country</label>
                        <select name="type" id="select-type" class="form-control sell-input">
                            <option value="">{{ __('-- Select --') }}</option>
                            <option value="">1</option>
                            <option value="">2</option>
                        </select>
                </div>
                <div class="col-md-6 mb-3">
                    <label for="validationDefault01">Property Type</label>
                    <select name="type" id="select-type" class="form-control sell-input">
                        <option value="">{{ __('-- Select --') }}</option>
                        <option value="">1</option>
                        <option value="">2</option>
                    </select>
                </div>
            </div>
            <div class="form-row">
                <div class="col-md-12">
                    <label for="validationDefault01">Message</label>
                    <textarea name="content" class="form-control sell-input" rows="5"
                              placeholder="{{ __('Message') }} *"></textarea>
                </div>
            </div>
            <div class="form-row mt-4">
                <div class="col-md-10">
                    <input type="checkbox" id="exampleCheck1">
                    <label class="form-check-label" for="exampleCheck1">I agree ti the <u>term of service and privacy
                            policy</u></label>
                </div>

                <div class="col-md-2">
                    <button type="submit" class="btn btn-primary btn-block btn-submit-form">Submit Form</button>
                </div>
            </div>

            </form>
        </div>
    </section>

    <section>
        <div class="container container-sell-content">
            <div class="sell-content">
                <h2 class="text-left color_basic">Who Is SAMA?</h2>
                <div class="container">
                    <p class="color_black sell-description">
                        SAMA Emirates Properties is a real estate company that helps individuals sell properties. Located in Abu Dhabi,
                        in the United Arab Emirates, SAMA Emirates Properties offers its services and real estate listings for all
                        individuals in the UAE. For over 20 years, SAMA Emirates Properties has proven that it has what it takes and
                        that it is a professional real estate brokerage company that is able to assist clients in
                        selling, buying, renting, and renting out properties across the country.
                        Thus, to sell properties, advertise properties, or for services such as real estate listings,
                        SAMA Emirates Properties is the leading real estate company in the UAE that is known for taking the burden off
                        its customers’ shoulders. All thanks to the professionals and experts in the team that consists
                        of real estate agents, traders, engineers, surveyors, advisors, consultants, and more. For SAMA
                        UAE, selling properties is a walk in the park.
                        On the flip side, the employees at SAMA Emirates Properties are always empowered, lifted, and valued, which
                        allows them to cherish the work they do and be the best version of themselves. A happy client is
                        always served by an even happier employee. Furthermore, SAMA Emirates Properties is one company that includes
                        other real estate companies in the UAE, giving clients the chance to turn their dreams into
                        reality.
                    </p>
                </div>
            </div>
            <div class="sell-content">
                <h2 class="text-left color_basic">What Can You Sell on SAMA? </h2>
                <div class="container">
                    <p class="color_black sell-description">To sell properties on SAMA Emirates Properties is to have faith that you are
                        getting one step closer. At SAMA Emirates Properties, nothing is ever rocket science, especially with the
                        extensive experience in the real estate industry. A real estate broker is always glad to help
                        clients sell properties, and at SAMA Emirates Properties, clients are always matched with a specialist who knows
                        the area best. Some examples of what individuals can sell on SAMA include the following:</p>
                    <div class="color_black sell-description">
                        <ol>
                            <li>Villa</li>
                            <li>Apartment</li>
                            <li>Studio</li>
                            <li>Hotel apartment</li>
                            <li>Townhouse</li>
                            <li>House</li>
                            <li>Penthouse</li>
                            <li>Office</li>
                            <li>Building</li>
                            <li>Compound</li>
                            <li>Warehouse</li>
                            <li>Lands/ spot</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section>
        <div class="container padtop40">
            <div class="row mt-5">
                <div class="col-md-3">
                    <img src="{{asset('storage/sell-with-us/Image_1.png')}}" class="img-area-type-1">
                    <div class="show-text-sell-with-us"> Villa</div>
                </div>
                <div class="col-md-3">
                    <img src="{{asset('storage/sell-with-us/Image_2.png')}}" class="img-area-type-2">
                    <div class="show-text-sell-with-us"> Apartment</div>
                </div>
                <div class="col-md-3">
                    <img src="{{asset('storage/sell-with-us/Image_3.png')}}" class="img-area-type-1">
                    <div class="show-text-sell-with-us"> Studio</div>
                </div>
                <div class="col-md-3">
                    <img src="{{asset('storage/sell-with-us/Image_4.png')}}" class="img-area-type-2">
                    <div class="show-text-sell-with-us"> Hotel apartment</div>
                </div>
            </div>
            <div class="row mt-5">
                <div class="col-md-3">
                    <img src="{{asset('storage/sell-with-us/Image_5.png')}}" class="img-area-type-1">
                    <div class="show-text-sell-with-us"> Townhouse</div>
                </div>
                <div class="col-md-3">
                    <img src="{{asset('storage/sell-with-us/Image_6.png')}}" class="img-area-type-2">
                    <div class="show-text-sell-with-us"> House</div>
                </div>
                <div class="col-md-3">
                    <img src="{{asset('storage/sell-with-us/Image_7.png')}}" class="img-area-type-1">
                    <div class="show-text-sell-with-us"> Penthouse</div>
                </div>
                <div class="col-md-3">
                    <img src="{{asset('storage/sell-with-us/Image_8x.png')}}" class="img-area-type-2">
                    <div class="show-text-sell-with-us"> Office</div>
                </div>
            </div>
            <div class="row mt-5">
                <div class="col-md-3">
                    <img src="{{asset('storage/sell-with-us/Image_9.png')}}" class="img-area-type-1">
                    <div class="show-text-sell-with-us"> Building</div>
                </div>
                <div class="col-md-3">
                    <img src="{{asset('storage/sell-with-us/Image_10.png')}}" class="img-area-type-2">
                    <div class="show-text-sell-with-us"> Compound</div>
                </div>
                <div class="col-md-3">
                    <img src="{{asset('storage/sell-with-us/Image_11.png')}}" class="img-area-type-1">
                    <div class="show-text-sell-with-us"> Warehouse</div>
                </div>
                <div class="col-md-3">
                    <img src="{{asset('storage/sell-with-us/Image_12.png')}}" class="img-area-type-2">
                    <div class="show-text-sell-with-us"> Lands/ spot</div>
                </div>
            </div>
        </div>
    </section>

    <section>
        <div class="container-fluid  padding-mask-group padding-mask-group-custom">
            <div class="row">
                <div class="col"><img src="/storage/mask-group-33.png"></div>
                <div class="col" style="text-align: center;"><img src="/storage/mask-group-33.png"></div>
                <div class="col" style="text-align: right;"><img src="/storage/mask-group-33.png"></div>
            </div>
        </div>
    </section>

    @if(app()->getLocale() == "en")
        <section>
            <div class="text-center color_basic_2 padding-mask-number">
                <h1>How Does It Work With SAMA ?</h1></div>
            <div class="container text-center about-us-section about-us-section-custom" style="width: 1400px;"><p><img
                        src="/storage/general/3_1.svg" alt="Group%204015%20(1).svg" class="line-1-custom"> <img
                        src="/storage/general/1_1.svg" alt="" class="line-2-custom"> <img src="/storage/general/2_2.svg"
                                                                                          alt="" class="line-3-custom">
                </p>
                <div class="row padding-10">
                    <div><img src="/storage/general/1.svg" alt="about1.png" class="number-icon"></div>
                    <div class="col"><p class="color_basic_2" style="font-size: 18px; text-align: left;">
                            Facility
                            management&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>-
                            Rental management&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>-
                            Property management</p></div>
                </div>
                <div class="row padding-10">
                    <div class="col" style="text-align: right;"><img src="/storage/general/2.svg" alt="about1.png"
                                                                     class="number-icon"></div>
                    <div class="col"><p class="color_basic_2" style="font-size: 18px; text-align: left;">
                            Negotiations&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>-
                            Property showcase&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>-
                            Listings&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>-
                            Process of selling, buying, renting, and renting out a property</p></div>
                </div>
                <div class="row padding-10">
                    <div><img src="/storage/general/3.svg" alt="about1.png" class="number-icon"></div>
                    <div class="col"><p class="color_basic_2" style="font-size: 18px; text-align: left;">
                            Facility
                            management&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>-
                            Rental management&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>-
                            Property management</p></div>
                </div>
                <div class="row padding-10">
                    <div class="col" style="text-align: right;"><img src="/storage/general/4.svg" alt="about1.png"
                                                                     class="number-icon"></div>
                    <div class="col"><p class="color_basic_2" style="font-size: 18px; text-align: left;">
                            Negotiations&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>-
                            Property showcase&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>-
                            Listings&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>-
                            Process of selling, buying, renting, and renting out a property</p></div>
                </div>
            </div>
        </section>
    @endif

    <section>
        <div class="sell-marketing">
            <div class="marketing-ads-section">
                <h1 style="color: rgb(255, 255, 255); text-align: center;">Marketing and Ads</h1>
                <section class="container marketing-text-box">
                    <div class="row" style="background-color: rgb(255, 255, 255);">
                        <div class="col">
                            <p class="color_black font_size_20 mb-5">When customers want to sell properties, they must
                                keep in mind the new technologies and trends that will either positively or negatively
                                affect their goal.<br>
                                That being said, SAMA Emirates Properties also offers services related to marketing and ads to help
                                customers sell properties. From consultancy, to different marketing strategies, and even
                                ads, SAMA Emirates Properties can study your needs, the needs of the market, and then work accordingly.
                            </p>
                            <p><a href="#" class="btn-markting">Sell with us now!</a></p>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </section>

    <section>

        <div class="text-center color_basic_2 consultancy-q consultancy-q-custom">
            <h1>What Services Does SAMA Offer ?</h1>
        </div>

        <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner">
                <div class="carousel-item carousel-item-2 active">
                    <div class="slide-sell-us d-block w-100 h-100">
                        <div class="container">
                            <div class="row">
                                <div class="about-us-wrap-custom padding-top-8">
                                    <h1 class="color_white font-bolder">Consultancy</h1>
                                    <div class="sell-content-carousel">
                                        <h4 class="font-bolder rgb-135-181-214">Facility Management</h4>
                                        <div class="container">
                                            <p class="color_white sell-description">
                                                Ensuring comfort, functionality, efficiency, sustainability,
                                                infrastructure, grounds, and safety of the building or property in the
                                                best way possible.</p></div>
                                    </div>
                                    <div class="sell-content-carousel">
                                        <h4 class="rgb-135-181-214 font-bolder">
                                            Investment Advisory
                                        </h4>
                                        <div class="container">
                                            <p class="color_white sell-description">Assisting customers in making
                                                crucial decisions related to buying, selling, and investing by offering
                                                advice from investment professionals and experts.
                                            </p></div>
                                    </div>
                                    <div class="sell-content-carousel">
                                        <h4 class="rgb-135-181-214 font-bolder">Property
                                            Management</h4>
                                        <div class="container">
                                            <p class="color_white sell-description">Operating, controlling, maintaining,
                                                and overseeing of real estate properties, whether it is residential,
                                                commercial, or even land real estate.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="carousel-item carousel-item-2">
                    <div class="slide-sell-us d-block w-100 h-100">
                        <div class="container">
                            <div class="row">
                                <div class="about-us-wrap-custom padding-top-8">
                                    <h1 class="color_white font-bolder">Consultancy</h1>
                                    <div class="sell-content-carousel">
                                        <h4 class="font-bolder rgb-135-181-214">Real estate consultancy</h4>
                                        <div class="container">
                                            <p class="color_white sell-description">
                                                Offering advice related to real estate properties, especially in
                                                financial matters and assisting clients, investors, lenders, developers,
                                                and more in making crucial financial decisions.</p></div>
                                    </div>
                                    <div class="sell-content-carousel">
                                        <h4 class="rgb-135-181-214 font-bolder">
                                            Property valuation
                                        </h4>
                                        <div class="container">
                                            <p class="color_white sell-description">Assisting customers in estimating
                                                the value of their property for rent or for sale. Property valuation can
                                                be offered to both buyers and sellers, based on their needs.</p></div>
                                    </div>
                                    <div class="sell-content-carousel">
                                        <h4 class="rgb-135-181-214 font-bolder">
                                            Negotiations</h4>
                                        <div class="container">
                                            <p class="color_white sell-description">Negotiating on behalf of customers
                                                and parties, especially when it comes to financial matters such as the
                                                price of a property that needs to be paid.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="carousel-item carousel-item-2">
                    <div class="slide-sell-us d-block w-100 h-100">
                        <div class="container">
                            <div class="row">
                                <div class="about-us-wrap-custom padding-top-8">
                                    <h1 class="color_white font-bolder">Consultancy</h1>
                                    <div class="sell-content-carousel">
                                        <h4 class="font-bolder rgb-135-181-214">Property showcase</h4>
                                        <div class="container">
                                            <p class="color_white sell-description">
                                                Showcasing the properties if owners want to sell properties or even if
                                                they want to rent out the properties to potential buyers and
                                                renters.</p></div>
                                    </div>
                                    <div class="sell-content-carousel"><h4 class="rgb-135-181-214 font-bolder">
                                            Process of selling, buying, renting, and renting out a property</h4>
                                        <div class="container">
                                            <p class="color_white sell-description">Assisting owners, tenants, and
                                                buyers in the process of selling, buying, renting, and renting out a
                                                property from A to Z.</p></div>
                                    </div>
                                    <div class="sell-content-carousel"><h4 class="rgb-135-181-214 font-bolder"> Selling
                                            and
                                            refinancing</h4>
                                        <div class="container">
                                            <p class="color_white sell-description">Assisting customers with the matters
                                                of paying off their existing mortgage and replacing it with a new loan
                                                with better terms, interest rates, and different payment schedule.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="carousel-item carousel-item-2">
                    <div class="slide-sell-us d-block w-100 h-100">
                        <div class="container">
                            <div class="row">
                                <div class="about-us-wrap-custom padding-top-8">
                                    <h1 class="color_white font-bolder">Consultancy</h1>
                                    <div class="sell-content-carousel">
                                        <h4 class="font-bolder rgb-135-181-214">Property maintenance</h4>
                                        <div class="container">
                                            <p class="color_white sell-description">
                                                Helping customers with the upkeep of an apartment, a house, a rental
                                                property, a building, or any other property.</p></div>
                                    </div>
                                    <div class="sell-content-carousel"><h4 class="rgb-135-181-214 font-bolder">
                                            Rental management</h4>
                                        <div class="container">
                                            <p class="color_white sell-description">Assisting customers in overseeing a
                                                rented real estate property such as day-to-day operations, screening
                                                tenants, and repairing damages.</p></div>
                                    </div>
                                    <div class="sell-content-carousel"><h4 class="rgb-135-181-214 font-bolder">
                                            Marketing and ads
                                        </h4>
                                        <div class="container">
                                            <p class="color_white sell-description">Offering customers services related
                                                to marketing the property and creating appropriate ads to target the
                                                right investors in the best way possible.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="carousel-item carousel-item-2">
                    <div class="slide-sell-us d-block w-100 h-100">
                        <div class="container">
                            <div class="row">
                                <div class="about-us-wrap-custom padding-top-8">
                                    <h1 class="color_white font-bolder">Consultancy</h1>
                                    <div class="sell-content-carousel">
                                        <h4 class="font-bolder rgb-135-181-214">Listings</h4>
                                        <div class="container">
                                            <p class="color_white sell-description">
                                                Assisting customers in writing a professional real estate listing
                                                description to be able to attract potential investors and to make sure
                                                to mention all important details.</p></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <a class="carousel-control-prev carousel-control-prev-2" href="#carouselExampleControls"
               role="button" data-slide="prev">
                <span><i style='font-size:24px' class='fas'>&#xf137;</i></span>
            </a>
            <ol class="carousel-indicators carousel-indicators-custom">
                <li data-target="#carouselExampleControls"
                    data-slide-to="0" class="active circle-slide">
                </li>
                <li data-target="#carouselExampleControls"
                    data-slide-to="1" class="circle-slide">
                </li>
                <li data-target="#carouselExampleControls"
                    data-slide-to="2" class="circle-slide">
                </li>
                <li data-target="#carouselExampleControls"
                    data-slide-to="3" class="circle-slide">
                </li>
                <li data-target="#carouselExampleControls"
                    data-slide-to="4" class="circle-slide">
                </li>
            </ol>
            <a class="carousel-control-next carousel-control-next-2" href="#carouselExampleControls"
               role="button" data-slide="next">
                <span><i style='font-size:24px' class='fas'>&#xf138;</i></span>
            </a>


        </div>
    </section>

    @if(app()->getLocale() == "en")
    <section>
        <div class="text-center color_basic_2 padding-mask-number">
            <h1>How Does It Work With SAMA ?</h1>
        </div>
        <div class="container text-center about-us-section about-us-section-custom" style=" width: 1400px;">
            <p>
                <img src="/storage/general/3_1.svg" alt="Group%204015%20(1).svg" class="line-1-custom">
                <img src="/storage/general/1_1.svg" alt="" class="line-2-custom">
                <img src="/storage/general/2_2.svg" alt="" class="line-3-custom">
            </p>
            <div class="row padding-10">
                <div class=""><img class="number-icon" src="/storage/general/1.svg" alt="about1.png"></div>
                <div class="col">
                    <p style="font-size: 18px;text-align: left;" class="color_basic_2">Fill out the form with <br> your
                        information.</p></div>
            </div>
            <div class="row padding-10">
                <div class="col" style="text-align: right;">
                    <img class="number-icon" src="/storage/general/2.svg" alt="about1.png"></div>
                <div class="col">
                    <p style="font-size: 18px;text-align: left;" class="color_basic_2"> Our team will review<br> your
                        request and <br>contact you shortly.</p></div>

            </div>
            <div class="row padding-10">
                <div class=""><img class="number-icon" src="/storage/general/3.svg" alt="about1.png"></div>
                <div class="col">
                    <p style="font-size: 18px;text-align: left;" class="color_basic_2">We will receive the<br> needed
                        information.</p></div>
            </div>
            <div class="row padding-10">
                <div class="col" style="text-align: right;">
                    <img class="number-icon" src="/storage/general/4.svg" alt="about1.png"></div>
                <div class="col">
                    <p style="font-size: 18px;text-align: left;" class="color_basic_2">Your property will be posted!</p>
                </div>

            </div>

        </div>
    </section>
    @endif

    <section>
        <div class="container-fluid padding-mask-group padding-mask-group-custom"
             style=";padding-right: 0;padding-left: 0;">
            <div class="row">
                <div class="col"><img src="/storage/mask-group-33.png"></div>
                <div class="col" style="text-align: center;"><img src="/storage/mask-group-33.png"></div>
                <div class="col" style="text-align: right;"><img src="/storage/mask-group-33.png"></div>
            </div>
        </div>
    </section>

    <section>
        {!! Theme::partial('our-blog') !!}
    </section>

    <section>
        {!! Theme::partial('frequently-searched') !!}
</section>

<script>
    const carousel = new bootstrap.Carousel('#myCarousel')
</script>
</div>
{!! Theme::partial('footer') !!}
