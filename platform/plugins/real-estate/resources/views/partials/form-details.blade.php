@push('header')
    <script>
        window.trans = JSON.parse('{!! addslashes(json_encode(trans('plugins/real-estate::dashboard'))) !!}');
    </script>
@endpush

<div id="app-real-estate">
    <div>
        <div class="content-details">
            @if(isset($details) && !empty($details))
                @foreach($details as $key=>$val)
                    <div class="form-group mb-3" >
                        <div class="row">
                            <div class="col-md-2 col-sm-5 pl-0">
                                <div class="form-group mb-3">
                                    <input type="text" value="{{$val["unit"]??''}}" name="details[][unit]"
                                           class="form-control" placeholder="Name">
                                </div>
                            </div>
                            <div class="col-md-2 col-sm-5">
                                <div class="form-group mb-3">
                                    <input type="number" min="0" value="{{$val["size"]??''}}"
                                           name="details[][size]"
                                           class="form-control" placeholder="Size">
                                </div>
                            </div>
                            <div class="col-md-2 col-sm-5">
                                <div class="form-group mb-3">
                                    <input type="number" min="0" value="{{$val["price"]??''}}"
                                           name="details[][price]" class="form-control"
                                           placeholder="Price">
                                </div>
                            </div>
                            <div class="col-md-2 col-sm-5">
                                <div class="form-group mb-3">
                                    <input type="text" value="{{$val["view"]??''}}" name="details[][view]"
                                           class="form-control" placeholder="View">
                                </div>
                            </div>
                            <div class="col-md-2 col-sm-2">
                                <button class="btn btn-warning" type="button" onclick="deleteRow()"><i
                                        class="fa fa-times"></i></button>
                            </div>
                        </div>
                    </div>
                @endforeach
            @else
                <div class="form-group mb-3">
                    <div class="row">
                        <div class="col-md-2 col-sm-5 pl-0">
                            <div class="form-group mb-3">
                                <input type="text" value="" name="details[][unit]" class="form-control"
                                       placeholder="Name">
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-5">
                            <div class="form-group mb-3">
                                <input type="number" min="0" value="" name="details[][size]" class="form-control"
                                       placeholder="Size">
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-5">
                            <div class="form-group mb-3">
                                <input type="number" min="0" value="" name="details[][price]" class="form-control"
                                       placeholder="Price">
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-5">
                            <div class="form-group mb-3">
                                <input type="text" value="" name="details[][view]" class="form-control"
                                       placeholder="View">
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-2">
                            <button class="btn btn-warning" type="button" onclick="deleteRow()"><i
                                    class="fa fa-times"></i></button>
                        </div>
                    </div>
                </div>
            @endif
        </div>
        <div class="form-group mb-3">
            <button class="btn btn-info" type="button" onclick="addRow()">{{ __('add_new') }}</button>
        </div>
    </div>
</div>


<script>

    $(document).on('click', '.btn-warning', function () {
        $(this).parents(".form-group").remove()
    });

    function addRow() {

        var html = $(".content-details").find(".form-group:first").clone()

        html.find("input").val('')
        html.find("input").eq(0).attr("name",'details[][unit]')
        html.find("input").eq(1).attr("name",'details[][size]')
        html.find("input").eq(2).attr("name",'details[][price]')
        html.find("input").eq(3).attr("name",'details[][view]')
        html.find("input").removeClass("is-valid")

        $(".content-details").append(html)
    }

</script>
