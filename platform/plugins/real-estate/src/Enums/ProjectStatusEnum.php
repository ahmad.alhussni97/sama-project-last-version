<?php

namespace Botble\RealEstate\Enums;

use Botble\Base\Supports\Enum;
use Html;

/**
 * @method static ProjectStatusEnum NOT_AVAILABLE()
 * @method static ProjectStatusEnum UNDER_CONSTRUCTION()
 * @method static ProjectStatusEnum COMPLETED()
 * @method static ProjectStatusEnum LAUNCHING_SOON()
 * @method static ProjectStatusEnum OFF_PLAN()
 * @method static ProjectStatusEnum SOLD()
 */
class ProjectStatusEnum extends Enum
{
    public const NOT_AVAILABLE = 'Not Available';
    public const UNDER_CONSTRUCTION = 'Under Construction';
    public const COMPLETED="Completed";
    public const LAUNCHING_SOON="Launching Soon";
    public const OFF_PLAN = 'Off-plan';
    public const SOLD = 'Sold';

    /**
     * @var string
     */
    public static $langPath = 'plugins/real-estate::project.statuses';

    /**
     * @return string
     */
    public function toHtml()
    {
        switch ($this->value) {
            case self::NOT_AVAILABLE:
                return Html::tag('span', self::NOT_AVAILABLE()->label(), ['class' => 'label-default status-label'])
                    ->toHtml();
            case self::UNDER_CONSTRUCTION:
                return Html::tag('span', self::UNDER_CONSTRUCTION()->label(), ['class' => 'label-warning status-label'])
                    ->toHtml();
            case self::COMPLETED:
                return Html::tag('span', self::COMPLETED()->label(), ['class' => 'label-success status-label'])
                    ->toHtml();
            case self::LAUNCHING_SOON:
                return Html::tag('span', self::LAUNCHING_SOON()->label(), ['class' => 'label-danger status-label'])
                    ->toHtml();
            case self::OFF_PLAN:
                return Html::tag('span', self::OFF_PLAN()->label(), ['class' => 'label-info status-label'])
                    ->toHtml();
            case self::SOLD:
                return Html::tag('span', self::SOLD()->label(), ['class' => 'label-info status-label'])
                    ->toHtml();
            default:
                return null;
        }
    }
}
