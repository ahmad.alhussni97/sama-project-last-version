$(".carousel-arrow").hover(function () {
    $(".arrow-slide").css("margin-left", "15%");
    $(".carousel-arrow").css("width", "80px");
    $(".carousel-arrow").css("height", "80px");
    $(".arrow-slide").css("padding-bottom", "0px");
    $(".hide").css("display", "block");
}, function () {
    $(".hide").css("display", "none");
    $(".carousel-arrow").css("width", "unset");
    $(".carousel-arrow").css("height", "unset");
    $(".arrow-slide").css("margin-left", "0%");
    $(".arrow-slide").css("padding-bottom", "10px");
})

var lengthPag = $("#pagination-project > nav > ul > li").length;
for (var i = 1; i < lengthPag - 2; i++) {
    $("#pagination-project > nav > ul > li").eq(i).append("<label class='page-space'>|</label>")
}

$(".read-more").click(function () {
    $(".collapse").toggle();
    $(".read-more").remove();
});


$(document).ready(function () {

    var url = '';

    $(document).on('click', '.shareButton', function (e) {
        e.preventDefault();
        url = $(this).attr('rel')
        $(".frame > .container").jsSocials({
            url: url,
            text: "SubScribe to the Socials",
            shares: ["email", "twitter", "facebook", "googleplus", "linkedin", "pinterest", "stumbleupon", "whatsapp"]
        });
        $("#myModal").show();
    });

    $(".frame > .container").jsSocials("destroy");
    $(".frame > .container").jsSocials("option", "showCount", false);
    $(".frame > .container").jsSocials("refresh");

    $(".close").click(function () {
        $("#myModal").hide();
    });

});

$('#recipeCarousel').carousel({
    interval: 10000
})

$('.carousel-project .carousel-item').each(function(){
    var minPerSlide = 4;
    var next = $(this).next();
    if (!next.length) {
        next = $(this).siblings(':first');
    }
    next.children(':first-child').clone().appendTo($(this));

    for (var i=0;i<minPerSlide;i++) {
        next=next.next();
        if (!next.length) {
            next = $(this).siblings(':first');
        }

        next.children(':first-child').clone().appendTo($(this));
    }
});

$(document).ready(function(){
    $('.carousel-inner-project').lightGallery();
});





























