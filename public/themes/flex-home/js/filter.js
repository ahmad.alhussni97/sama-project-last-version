
function displayAdvancedFilter(){

    $("#filter_modal").hide();
    $("#location_id").val("-1")
    $("#property-custom").val("-1")
    $("#min_price_1").val('')
    $("#max_price_1").val('')
    $("#advanced-filter-modal").show();

}

function closeAdvancedFilter(){

    $("#advanced-filter-modal").hide();
    $("#location_id_2").val("-1")
    $("#property-custom_2").val("-1")
    $("#filter_modal").show();

}


function displayAdvancedFilterArea(){
    $("#filter_modal").hide();
    $("#select-category").val("-1")
    $("#advanced-filter-modal").show();
}

function closeAdvancedFilterArea(){
    $("#advanced-filter-modal").hide();
    $("#property-custom_2").val("-1")
    $("#filter_modal").show();
}

$(".js-range-slider").ionRangeSlider({
    skin:"round",
    type:"double",
    hide_min_max:"true",
});

$("#features :input").each(function () {
    el = $(this);
    if (this.checked) {
        el.addClass("checked-btn");

        el.css("background-color", "red");

    }
});

$(".plus-1").click(function () {
    val = $("#input-counter-1").val();
    val = parseInt(val);
    $("#input-counter-1").val(val + 1);
});

$(".plus-2").click(function () {
    val = $("#input-counter-2").val();
    val = parseInt(val);
    $("#input-counter-2").val(val + 1);
});

$(".minus-1").click(function () {
    val = $("#input-counter-1").val();
    val = parseInt(val);
    $("#input-counter-1").val(val - 1);
});

$(".minus-2").click(function () {
    val = $("#input-counter-2").val();
    val = parseInt(val);
    $("#input-counter-2").val(val - 1);
});


$("#features").on('click', 'label', function () {
    {
        if ($(this).hasClass("checked-feature")) {
            $(this).removeClass("checked-feature")
            $(this).addClass("unchecked-feature")
            id = $(this).attr("id")
            $("."+id).prop('checked', false);
        } else {
            $(this).addClass("checked-feature")
            $(this).removeClass("unchecked-feature")
            id = $(this).attr("id")
            $("."+id).prop('checked', true);
        }
    }
});

$("#currency").on('click', 'label', function () {
    {
        if ($(this).hasClass("checked-feature")) {
            $(this).removeClass("checked-feature")
            $(this).addClass("unchecked-feature")
        } else {
            $(this).addClass("checked-feature")
            $(this).removeClass("unchecked-feature")
        }

    }
});


$("#close-custom").on('click', function () {
    {
        $("#advanced-filter-modal").hide();
        $("#filter_modal").show();
    }
});


$("#advanced_filter").click(function () {
   $(".result-data").empty()
});
